<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSAdminControls_ContentRating_Controls_Stars" Codebehind="Stars.ascx.cs" %>
<ajaxToolkit:Rating ID="elemRating" runat="server"
    StarCssClass="ratingStar"
    WaitingStarCssClass="savedRatingStar"
    FilledStarCssClass="filledRatingStar"
    EmptyStarCssClass="emptyRatingStar"
    />
