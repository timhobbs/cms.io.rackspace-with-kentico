﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------



public partial class CMSAPIExamples_Default {
    
    /// <summary>
    /// layoutElem control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.UIControls.UILayout layoutElem;
    
    /// <summary>
    /// paneHeader control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.UIControls.UILayoutPane paneHeader;
    
    /// <summary>
    /// cmsdesktop control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.UIControls.UILayoutPane cmsdesktop;
}
