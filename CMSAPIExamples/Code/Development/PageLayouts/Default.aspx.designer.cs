﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.4952
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------



public partial class CMSAPIExamples_Code_Development_PageLayouts_Default
{
    
    /// <summary>
    /// pnlCreateLayout control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.UIControls.APIExamplePanel pnlCreateLayout;
    
    /// <summary>
    /// apiCreateLayout control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiCreateLayout;
    
    /// <summary>
    /// apiGetAndUpdateLayout control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiGetAndUpdateLayout;
    
    /// <summary>
    /// apiGetAndBulkUpdateLayouts control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiGetAndBulkUpdateLayouts;
    
    /// <summary>
    /// pnlDeleteLayout control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.UIControls.APIExamplePanel pnlDeleteLayout;
    
    /// <summary>
    /// apiDeleteLayout control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiDeleteLayout;
}
