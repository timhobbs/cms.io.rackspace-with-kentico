﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.4961
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------



public partial class CMSAPIExamples_Code_Community_Messaging_Default
{
    
    /// <summary>
    /// pnlCreateMessage control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.UIControls.APIExamplePanel pnlCreateMessage;
    
    /// <summary>
    /// apiCreateMessage control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiCreateMessage;
    
    /// <summary>
    /// apiGetAndUpdateMessage control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiGetAndUpdateMessage;
    
    /// <summary>
    /// apiGetAndBulkUpdateMessages control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiGetAndBulkUpdateMessages;
    
    /// <summary>
    /// pnlAddUserToContactList control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.UIControls.APIExamplePanel pnlAddUserToContactList;
    
    /// <summary>
    /// apiAddUserToContactList control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiAddUserToContactList;
    
    /// <summary>
    /// pnlAddUserToIgnoreList control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.UIControls.APIExamplePanel pnlAddUserToIgnoreList;
    
    /// <summary>
    /// apiAddUserToIgnoreList control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiAddUserToIgnoreList;
    
    /// <summary>
    /// pnlDeleteMessage control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.UIControls.APIExamplePanel pnlDeleteMessage;
    
    /// <summary>
    /// apiDeleteMessage control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiDeleteMessage;
    
    /// <summary>
    /// pnlRemoveUserFromContactList control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.UIControls.APIExamplePanel pnlRemoveUserFromContactList;
    
    /// <summary>
    /// apiRemoveUserFromContactList control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiRemoveUserFromContactList;
    
    /// <summary>
    /// pnlRemoveUserFromIgnoreList control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.UIControls.APIExamplePanel pnlRemoveUserFromIgnoreList;
    
    /// <summary>
    /// apiRemoveUserFromIgnoreList control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiRemoveUserFromIgnoreList;
}
