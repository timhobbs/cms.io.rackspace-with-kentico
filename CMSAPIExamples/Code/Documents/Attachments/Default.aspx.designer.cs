﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.4961
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------



public partial class CMSAPIExamples_Code_Documents_Attachments_Default
{
    
    /// <summary>
    /// pnlPreparation control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.UIControls.APIExamplePanel pnlPreparation;
    
    /// <summary>
    /// apiCreateExampleDocument control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiCreateExampleDocument;
    
    /// <summary>
    /// pnlInsertAttachment control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.UIControls.APIExamplePanel pnlInsertAttachment;
    
    /// <summary>
    /// apiInsertUnsortedAttachment control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiInsertUnsortedAttachment;
    
    /// <summary>
    /// apiInsertFieldAttachment control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiInsertFieldAttachment;
    
    /// <summary>
    /// pnlManageAttachments control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.UIControls.APIExamplePanel pnlManageAttachments;
    
    /// <summary>
    /// apiMoveAttachmentDown control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiMoveAttachmentDown;
    
    /// <summary>
    /// apiMoveAttachmentUp control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiMoveAttachmentUp;
    
    /// <summary>
    /// apiEditMetadata control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiEditMetadata;
    
    /// <summary>
    /// pnlDeleteDocumentAlias control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.UIControls.APIExamplePanel pnlDeleteDocumentAlias;
    
    /// <summary>
    /// apiDeleteAttachments control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiDeleteAttachments;
    
    /// <summary>
    /// apiDeleteExampleDocument control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAPIExamples_Controls_APIExample apiDeleteExampleDocument;
}
