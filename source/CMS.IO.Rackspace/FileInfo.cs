﻿using System;
using System.Collections.Generic;

using CMS.IO;
using CMS.IO.Rackspace.Adapter;
using CMS.GlobalHelper;

using Rackspace.Cloudfiles;
using rs = Rackspace.Cloudfiles.Constants;


namespace CMS.IO.Rackspace
{
    /// <summary>
    /// Sample of FileInfo class of CMS.IO provider.
    /// </summary>
    public class FileInfo : CMS.IO.FileInfo
    {
        #region "Variables"

        private const string LastAccessTimeHeadersString = "date";
        private readonly IContainerFactory mAdapterFactory;
        private Container mContainer;
        private string mFullName = String.Empty;
        private bool mExists = false;
        private DateTime mCreationTime = ZeroTimeUTC();
        private DateTime mLastAccessTime = ZeroTimeUTC();
        private DateTime mLastWriteTime = ZeroTimeUTC();
        private long mLength = 0;
        private bool mIsReadOnly = false;
        private CMS.IO.FileAttributes mAttributes;
        private StorageObject mStorageObject = null;
        private Metadata mMetadata = new Metadata();

        #endregion


        #region "Private methods"

        /// <summary>
        /// Gets RackSpace Storage Object from specified path
        /// </summary>
        /// <param name="path">Path</param>
        /// <returns></returns>
        private StorageObject GetStorageObject(string path)
        {
            try
            {
                string cloudPath = Helpers.GetCloudfilesPath(path);
                return mContainer.GetObject(Helpers.GetEncodedCloudfilesPath(cloudPath));
            }
            catch (ObjectNotFoundException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new System.IO.IOException(e.Message);
            }
        }

        #endregion


        #region "Constructors"

        /// <summary>
        /// Initializes new instance of
        /// </summary>
        /// <param name="filename">File name.</param>
        public FileInfo(string filename)
        {
            Helpers.ValidatePath(filename);

            mAdapterFactory = new CFContainerFactory();
            mContainer = mAdapterFactory.GetContainer();
            
            mFullName = filename;
            mAttributes = CMS.IO.FileAttributes.Normal;
            mMetadata.Creation_Time = mCreationTime;
            mMetadata.Last_Access_Time = mLastAccessTime;
            mMetadata.Last_Write_Time = mLastWriteTime;
            mMetadata.Read_Only = mIsReadOnly;

            try
            {
                mStorageObject = GetStorageObject(filename);
                mExists = true;
            }
            catch (ObjectNotFoundException)
            { }

            //initialize if object exists in Rackspace
            if (mExists)
            {
                Metadata metadata = Metadata.Parse(mStorageObject.Metadata);
                mLastAccessTime = metadata.Last_Access_Time;
                mLastWriteTime = metadata.Last_Write_Time;
                mCreationTime = metadata.Creation_Time;
                mLength = mStorageObject.ContentLength;
                mIsReadOnly = metadata.Read_Only;
                mAttributes = (FileAttributes)metadata.File_Attributes;
            }

            //CMS.IO.File.LogFileOperation(fullName, "constructor");
        }

        #endregion


        #region "Properties"

        /// <summary>
        /// Length of file.
        /// </summary>
        public override long Length
        {
            get
            {  
                return mLength;
            }
        }


        /// <summary>
        /// File extension.
        /// </summary>
        public override string Extension
        {
            get
            {
                return CMS.IO.Path.GetExtension(FullName);
            }
        }


        /// <summary>
        /// Full name of file (with whole path).
        /// </summary>
        public override string FullName
        {
            get
            {
                return mFullName;
            }
        }


        /// <summary>
        /// File name of file (without path).
        /// </summary>
        public override string Name
        {
            get
            {
                return CMS.IO.Path.GetFileName(FullName);
            }
        }


        /// <summary>
        /// Last write time to file.
        /// </summary>
        public override DateTime LastWriteTime
        {
            get
            {
                return mLastWriteTime;
            }

            set
            {
                mLastWriteTime = value;
                mMetadata.Last_Write_Time = value;
                mStorageObject.SyncMetadata(mMetadata.ToDictionary());   
            }
        }


        /// <summary>
        /// If file exists.
        /// </summary>
        public override bool Exists
        {
            get
            {
                return mExists;
            }
        }


        /// <summary>
        /// Creation date of file.
        /// </summary>
        public override DateTime CreationTime
        {
            get
            {
                 return mMetadata.Creation_Time;
            }

            set
            {
                mCreationTime = value;
                mMetadata.Creation_Time = value;
                mStorageObject.SyncMetadata(mMetadata.ToDictionary());
            }
        }


        /// <summary>
        /// Directory of file.
        /// </summary>
        public override CMS.IO.DirectoryInfo Directory
        {
            get
            {
                return new DirectoryInfo(CMS.IO.Path.GetDirectoryName(FullName));
            }
        }


        /// <summary>
        ///  If is read only.
        /// </summary>
        public override bool IsReadOnly
        {
            get
            {
                return mIsReadOnly;
            }

            set
            {
                mIsReadOnly = value;
                mMetadata.Read_Only = value;
                mStorageObject.SyncMetadata(mMetadata.ToDictionary());
            }
        }


        /// <summary>
        /// File attributes.
        /// </summary>
        public override CMS.IO.FileAttributes Attributes
        {
            get
            {
                return mAttributes;
            }

            set
            {
                mAttributes = value;
                mMetadata.File_Attributes = (int)mAttributes;
                mStorageObject.SyncMetadata(mMetadata.ToDictionary());
            }
        }


        /// <summary>
        /// Directory name.
        /// </summary>
        public override string DirectoryName
        {
            get
            {
                return CMS.IO.Path.GetDirectoryName(FullName);
            }
        }


        /// <summary>
        /// Last access time.
        /// </summary>
        public override DateTime LastAccessTime
        {
            get
            {
                return mMetadata.Last_Access_Time;
            }
            set
            {
                mLastAccessTime = value;
                mMetadata.Last_Access_Time = value;
                mStorageObject.SyncMetadata(mMetadata.ToDictionary());
            }
        }        

        #endregion


        #region "Methods"

        /// <summary>
        /// Creates or opens a file for writing UTF-8 encoded text.
        /// </summary> 
        public override CMS.IO.StreamWriter CreateText()
        {
            if (IsReadOnly)
            {
                throw new UnauthorizedAccessException("Access to file denied.");
            }

            File file = new File(mAdapterFactory);
            return file.CreateText(mFullName);
        }


        /// <summary>
        /// Deletes file.
        /// </summary>
        public override void Delete()
        {
            if (IsReadOnly)
            {
                throw new UnauthorizedAccessException("Access to file denied.");
            }
            if (!Exists)
            {
                throw new System.IO.IOException(String.Format("{0} not found", mFullName));
            }

            File file = new File(mAdapterFactory);
            file.Delete(FullName);
            mExists = false;
        }


        /// <summary>
        /// Creates a read-only ICMSFileStream.
        /// </summary> 
        public override CMS.IO.FileStream OpenRead()
        {
            if (!Exists)
            {
                throw new System.IO.IOException(String.Format("{0} not found", mFullName));
            }

            File file = new File(mAdapterFactory);
            return file.OpenRead(FullName);
        }


        /// <summary>
        /// Copies current file to destination. 
        /// </summary>
        /// <param name="destFileName">Destination file name.</param>
        public override CMS.IO.FileInfo CopyTo(string destFileName)
        {
            if (destFileName == null)
            {
                throw new ArgumentNullException("destFileName");
            }
            if (String.IsNullOrWhiteSpace(destFileName))
            {
                throw new ArgumentException("can't be blank", "destFileName");
            }
            if (!Exists)
            {
                throw new System.IO.IOException(String.Format("{0} not found", mFullName));
            }

            File file = new File(mAdapterFactory);
            file.Copy(FullName, destFileName);

            return new FileInfo(destFileName);
        }


        /// <summary>
        /// Copies current file to destination. 
        /// </summary>
        /// <param name="destFileName">Destination file name.</param>
        /// <param name="overwrite">Indicates if existing file should be overwritten</param>        
        public override CMS.IO.FileInfo CopyTo(string destFileName, bool overwrite)
        {
            if (destFileName == null)
            {
                throw new ArgumentNullException("destFileName");
            }
            if (String.IsNullOrWhiteSpace(destFileName))
            {
                throw new ArgumentException("can't be blank", "destFileName");
            }
            if (!Exists)
            {
                throw new System.IO.IOException(String.Format("{0} not found", mFullName));
            }

            File file = new File(mAdapterFactory);
            file.Copy(FullName, destFileName, overwrite);
            return new FileInfo(destFileName);
        }


        /// <summary>
        /// Moves an existing file to a new file.
        /// </summary>
        /// <param name="destFileName">Destination file name.</param>        
        public override void MoveTo(string destFileName)
        {
            if (destFileName == null)
            {
                throw new ArgumentNullException("destFileName");
            }
            if (String.IsNullOrWhiteSpace(destFileName))
            {
                throw new ArgumentException("can't be blank", "destFileName");
            }
            if (!Exists)
            {
                throw new System.IO.IOException(String.Format("{0} not found", mFullName));
            }

            File file = new File(mAdapterFactory);
            file.Move(FullName, destFileName);
            mFullName = destFileName;
        }


        /// <summary>
        /// Creates a StreamReader with UTF8 encoding that reads from an existing text file.
        /// </summary>  
        public override StreamReader OpenText()
        {
            if (!Exists)
            {
                throw new System.IO.IOException(String.Format("{0} not found", mFullName));
            }

            File file = new File(mAdapterFactory);
            return file.OpenText(mFullName);
        }

        #endregion


        #region "Private methods"

        private static DateTime ZeroTimeUTC()
        {
            return new DateTime(1601, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        }

        #endregion
    }
}
