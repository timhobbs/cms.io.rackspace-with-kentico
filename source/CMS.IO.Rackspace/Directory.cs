﻿using System;
using System.Linq;
using System.Security.AccessControl;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using CMS.IO;
using CMS.IO.Rackspace.Adapter;
using CMS.SettingsProvider;

using Rackspace.Cloudfiles;


namespace CMS.IO.Rackspace
{
    /// <summary>
    /// Sample of Directory class of CMS.IO provider.
    /// </summary>
    public class Directory : CMS.IO.AbstractDirectory
    {
        #region "Variables"
        
        private IContainerFactory mAdapterFactory;

        #endregion


        #region "Constructors"

        public Directory () : this(new CFContainerFactory())
        { }


        public Directory(IContainerFactory factory)
        {
            mAdapterFactory = factory;
        }

        #endregion


        #region "Public override methods"

        /// <summary>
        /// Determines whether the given path refers to an existing directory on disk.
        /// </summary>
        /// <param name="path">Path to test.</param>  
        public override bool Exists(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                return false;
            }

            // here will be verification if directory exists in cache or not

            // get path as is saved in Rackspace cloud
            string cloudPath = Helpers.GetCloudfilesDirectoryPath(path);
            
            Container container = mAdapterFactory.GetContainer();

            try
            {
                container.GetObject(Helpers.GetEncodedCloudfilesPath(cloudPath));
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }


        /// <summary>
        /// Creates all directories and subdirectories as specified by path.
        /// </summary>
        /// <param name="path">Path to create.</param> 
        public override CMS.IO.DirectoryInfo CreateDirectory(string path)
        {
            Helpers.ValidatePath(path);
            Helpers.ValidateDirectoryPathLength(path);

            // get directory path as is saved in Rackspace cloud
            string cloudfilesPath = Helpers.GetCloudfilesDirectoryPath(path);

            // Get path parts
            string[] pathParts = cloudfilesPath
                .Split(CFInfo.DELIMITER)
                .Where(p => !String.IsNullOrEmpty(p))
                .ToArray();

            Container container = mAdapterFactory.GetContainer();

            string tmpPath = String.Empty;
            // Creates all subdirectories and finally directory
            foreach (var part in pathParts)
            {
                tmpPath += part + CFInfo.DELIMITER;
                if (!Exists(tmpPath))
                {
                    StorageObject obj = container.CreateObject(Helpers.GetEncodedCloudfilesPath(tmpPath));
                    obj.Write(new System.IO.MemoryStream());
                    obj.SyncHeaders(Metadata.Default.ToDictionary());
                }
            }

            return new DirectoryInfo(path);
        }


        /// <summary>
        /// Returns the names of files (including their paths) in the specified directory.
        /// </summary>
        /// <param name="path">Path to retrieve files from.</param> 
        public override string[] GetFiles(string path)
        {
            return GetFiles(path, "*");
        }


        /// <summary>
        /// Returns the names of files (including their paths) in the specified directory.
        /// </summary>
        /// <param name="path">Path to retrieve files from.</param> 
        /// <param name="searchPattern">Search pattern.</param>
        public override string[] GetFiles(string path, string searchPattern)
        {
            Helpers.ValidatePath(path);
            Helpers.ValidateDirectoryExists(path, this);

            if (searchPattern.EndsWith("..")
                || searchPattern.Contains("..")
                || searchPattern.Contains(CFInfo.DELIMITER))
            {
                throw new ArgumentException("does not contain a valid pattern", "searchPattern");
            }

            string cloudPath = Helpers.GetCloudfilesDirectoryPath(path);

            Container container = mAdapterFactory.GetContainer();

            Dictionary<ObjectQuery, string> query = new Dictionary<ObjectQuery, string>();
            query.Add(ObjectQuery.Marker, Helpers.GetEncodedCloudfilesPath(cloudPath));

            List<Dictionary<string, string>> objectList = container.GetObjectList(query);

            Regex searchRegex = SearchPatternToSearchRegex(searchPattern);


            IEnumerable<String> fileNames = objectList
                .Where(o => o["name"].StartsWith(cloudPath) && !o["name"].Equals(cloudPath))
                .Select(o => o["name"])
                .Select(fileFullName => fileFullName.Remove(0, cloudPath.Length)) // remove path from start of name
                .Where(fileName => !fileName.Contains(CFInfo.DELIMITER))     // remove all names, which are not files
                .Where(fileName => searchRegex.IsMatch(fileName));           // apply searchPattern string

            // append backslash delimiter to path if path doesn't end with it
            path += path.EndsWith("\\") ? String.Empty : "\\";

            return fileNames.Select(fileName => path + fileName).ToArray();
        }


        /// <summary>
        /// Gets the names of subdirectories in the specified directory.
        /// </summary>
        /// <param name="path">Path to retrieve directories from.</param> 
        public override string[] GetDirectories(string path)
        {
            return GetDirectories(path, "*");
        }


        /// <summary>
        /// Gets the names of subdirectories in the specified directory.
        /// </summary>
        /// <param name="path">Path to retrieve directories from.</param> 
        /// <param name="searchPattern">Pattern to be searched.</param> 
        public override string[] GetDirectories(string path, string searchPattern)
        {
            Helpers.ValidatePath(path);
            if (searchPattern == null)
            {
                throw new ArgumentNullException("searchPattern");
            }

            Helpers.ValidateDirectoryExists(path, this);
            
            if (searchPattern.EndsWith("..")
                || searchPattern.Contains("..")
                || searchPattern.Contains(CFInfo.DELIMITER))
            {
                throw new ArgumentException("does not contain a valid pattern", "searchPattern");
            }

            Container container = mAdapterFactory.GetContainer();
            string cloudPath = Helpers.GetCloudfilesDirectoryPath(path);

            Dictionary<ObjectQuery, string> query = new Dictionary<ObjectQuery, string>();
            query.Add(ObjectQuery.Marker, Helpers.GetEncodedCloudfilesPath(cloudPath));

            List<Dictionary<string, string>> objectList = container.GetObjectList(true, query);

            Regex searchRegex = SearchPatternToSearchRegex(searchPattern);

            IEnumerable<string> directoryNames = objectList
                .Where(o => o["name"].StartsWith(cloudPath) && !o["name"].Equals(cloudPath))
                .Select(o => o["name"])
                // remove path from start of name
                .Select(directoryFullName => directoryFullName.Remove(0, cloudPath.Length))
                // get files, which represent directory and the directory is first-level subdirectory
                .Where(directoryName => 
                    directoryName.EndsWith(CFInfo.DELIMITER.ToString())       // directoryName ends with DELIMITER and at
                    && directoryName.Count(ch => ch == CFInfo.DELIMITER) == 1)// the same time count of DELIMITER equals to 1
                .Select(o => o.TrimEnd(CFInfo.DELIMITER))                     // remove trailing delimiter character
                .Where(directoryName => searchRegex.IsMatch(directoryName));  // apply searchPattern string

            // append backslash delimiter to path if path doesn't end with it
            path += path.EndsWith("\\") ? String.Empty : "\\";

            return directoryNames.Select(directoryName => path + directoryName).ToArray();
        }


        /// <summary>
        /// Gets the current working directory of the application.
        /// </summary>  
        public override string GetCurrentDirectory()
        {
            return SettingsKeyProvider.WebApplicationPhysicalPath;
            //return System.IO.Directory.GetCurrentDirectory();
        }


        /// <summary>
        /// Deletes an empty directory and, if indicated, any subdirectories and files in the directory.
        /// </summary>
        /// <param name="path">Path to directory</param>
        /// <param name="recursive">If delete if sub directories exists.</param>
        public override void Delete(string path, bool recursive)
        {
            Helpers.ValidatePath(path);
            Helpers.ValidateDirectoryExists(path, this);
            
            if (!Empty(path) && !recursive)
            {
                throw new System.IO.IOException(String.Format("{0} directory is not empty", path));
            }

            Container container = mAdapterFactory.GetContainer();
            
            foreach (var subfile in GetFiles(path))
            {
                new File(mAdapterFactory).Delete(subfile);
            }

            foreach (var subdir in GetDirectories(path))
            {
                Delete(subdir, true);
            }

            try
            {
                container.DeleteObject(Helpers.GetEncodedCloudfilesPath(
                    Helpers.GetCloudfilesDirectoryPath(path)));
            }
            catch (Exception e)
            {
                throw new System.IO.IOException(e.Message);
            }
        }


        /// <summary>
        /// Moves directory.
        /// </summary>
        /// <param name="sourceDirName">Source directory name.</param>
        /// <param name="destDirName">Destination directory name.</param>
        public override void Move(string sourceDirName, string destDirName)
        {
            Helpers.ValidatePath(sourceDirName);
            Helpers.ValidatePath(destDirName);

            // Invalid length of file names
            Helpers.ValidateDirectoryPathLength(sourceDirName);
            Helpers.ValidateDirectoryPathLength(destDirName);

            Helpers.ValidateDirectoryExists(sourceDirName, this);
            if (sourceDirName == destDirName)
            {
                throw new System.IO.IOException("sourceDirName and destDirName refers to the same directory");
            }
            if (Exists(destDirName))
            {
                throw new System.IO.IOException(String.Format("{0} already exists", destDirName));
            }

            CheckFilesPathLengthInNewDirectory(destDirName, sourceDirName);

            File file = new File(mAdapterFactory);
            MoveSubDirectories(file, sourceDirName, destDirName);
        }


        /// <summary>
        /// Deletes an empty directory.
        /// </summary>
        /// <param name="path">Path to directory</param>        
        public override void Delete(string path)
        {
            Delete(path, false);
        }


        /// <summary>
        /// Gets a FileSecurity object that encapsulates the access control list (ACL) entries for a specified file.
        /// </summary>
        /// <param name="path">Path to directory.</param>        
        public override DirectorySecurity GetAccessControl(string path)
        {
            Helpers.ValidatePath(path);
            Helpers.ValidateDirectoryExists(path, this);

            return new DirectorySecurity();
        }


        /// <summary>
        /// Prepares files for import. Converts them to media library.
        /// </summary>
        /// <param name="path">Path.</param>
        public override void PrepareFilesForImport(string path)
        {
            // It does nothing becouse of filesand directories are stored in lower case form.
        }


        /// <summary>
        /// Deletes all files in the directory structure. It works also in a shared hosting environment.
        /// </summary>
        /// <param name="path">Full path of the directory to delete</param>
        public override void DeleteDirectoryStructure(string path)
        {
            Delete(path, true);
        }

        #endregion


        #region "Private methods"

        /// <summary>
        /// Recursively moves files and directories
        /// </summary>
        private void MoveSubDirectories(File file, string sourceDirPath, string newDirPath)
        {
            if (file == null)
            {
                throw new ArgumentNullException("file");
            }
            if (newDirPath == null)
            {
                throw new ArgumentNullException("newDirPath");
            }
            if (sourceDirPath == null)
            {
                throw new ArgumentNullException("sourceDirName");
            }

            CreateDirectory(newDirPath);

            foreach (string directoryName in GetDirectories(sourceDirPath))
            {
                //correction of path
                if (newDirPath.EndsWith("\\"))
                {
                    newDirPath = newDirPath.Remove(newDirPath.LastIndexOf("\\") - 1);
                }
                MoveSubDirectories(file, directoryName, newDirPath + directoryName.Substring(sourceDirPath.Length));
            }

            foreach (string fileName in GetFiles(sourceDirPath))
            {
                newDirPath += newDirPath.EndsWith("\\") ? String.Empty : "\\";
                file.Move(fileName, newDirPath + new FileInfo(fileName).Name);
            }

            //delete after copying to dest
            Delete(sourceDirPath);
        }


        /// <summary>
        /// It checks path length of files, which will be moved from directory to an another directory.
        /// </summary>
        /// <param name="nDir">current directory, which will be checked</param>
        /// <param name="cDir"></param>
        private void CheckFilesPathLengthInNewDirectory(string nDirPath, string cDirPath)
        {
            if (nDirPath == null)
            {
                throw new ArgumentNullException("nDirPath");
            }
            if (cDirPath == null)
            {
                throw new ArgumentNullException("cDirPath");
            }

            nDirPath += nDirPath.EndsWith("\\") ? String.Empty : "\\";

            string[] files = GetFiles(cDirPath);
            foreach (string file in files)
            {
                Helpers.ValidateDirectoryPathLength(nDirPath + new FileInfo(file).Name);
            }

            string[] directories = GetDirectories(cDirPath);
            foreach (string directory in directories)
            {
                Helpers.ValidateDirectoryPathLength(nDirPath + new DirectoryInfo(cDirPath).Name);
                CheckFilesPathLengthInNewDirectory(nDirPath + new DirectoryInfo(directory).Name, directory);
            }
        }


        /// <summary>
        /// It checks if directory in specified path is empty or not.
        /// </summary>
        /// <param name="path">directory path</param>
        /// <returns>true if directory is empty, false otherwise</returns>
        private bool Empty(string path)
        {
            if (path == null)
            {
                throw new ArgumentNullException("path");
            }

            return GetFiles(path).Length == 0 && GetDirectories(path).Length == 0;
        }


        /// <summary>
        /// It creates search regular expression from searchPattern parameter,
        /// which occure within GetFiles and GetDirectories methods.
        /// </summary>
        /// <param name="searchPattern">search pattern string</param>
        /// <returns>search pattern regular expression</returns>
        private Regex SearchPatternToSearchRegex(string searchPattern)
        {
            if (searchPattern == null)
            {
                throw new ArgumentNullException("searchPattern");
            }

            return new Regex("^" + Regex.Escape(searchPattern)
                .Replace("\\*", ".*")
                .Replace("\\?", ".") + "$",
                RegexOptions.IgnoreCase);
        }

        #endregion
    }
}
