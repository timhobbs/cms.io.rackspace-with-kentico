﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using CMS.IO.Rackspace.Adapter;
using CMS.SettingsProvider;
using Rackspace.Cloudfiles;

namespace CMS.IO.Rackspace
{
    public static class Helpers
    {
        #region Methods

        /// <summary>
        /// Returns temporary directory e.g. C:\\tmp\\
        /// </summary>
        /// <returns>Temporary directory</returns>
        public static string GetTempDirectory()
        {
            return SettingsKeyProvider.GetStringValue("CF_TempDir");
        }


        /// <summary>
        /// It transforms all of backslash character to CFInfo.DELIMITER character.
        /// 
        /// Given path parameter can be in forms, which are mentioned below:
        /// 1. c:\file
        /// 2. c:/file
        /// 3. or any combination of slash and backslach as path delimiters.
        /// 
        /// Result still is the same: c:/file.
        /// 
        /// Assuming the CFInfo.DELIMITER is equal to slash.
        /// </summary>
        /// <param name="path">path with backslash characters</param>
        /// <returns>transformed path with CFInfo.DELIMITER charasters</returns>
        public static string GetCloudfilesPath(string path)
        {
            if (String.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentNullException("path");
            }
            //string current = new Directory().GetCurrentDirectory();
            //path = path.StartsWith(current) ? path.Substring(current.Length) : path;

            return path.ToLowerInvariant().Replace('\\', CFInfo.DELIMITER);
        }


        /// <summary>
        /// It transform all of backslash character to CFInfo.DELIMITER character
        /// and one more adds to end.
        /// 
        /// Given path parameter can be in forms, which are mentioned below:
        /// 1. c:\dir
        /// 2. c:\dir\
        /// 3. c:/dir
        /// 4. c:/dir/
        /// 5. or any combination of slash and backslach as path delimiters.
        /// 
        /// Result still is the same: c:/dir/.
        /// 
        /// Assuming the CFInfo.DELIMITER is equal to slash.
        /// </summary>
        /// <param name="path">path with backslash characters</param>
        /// <returns>transformed path with CFInfo.DELIMITER charasters, result ends with CFInfo.DELIMITER character</returns>
        public static string GetCloudfilesDirectoryPath(string path)
        {
            string dirPath = path;
            if (!path.EndsWith(CFInfo.DELIMITER.ToString()))
            {
                dirPath += path.EndsWith("\\") ? String.Empty : "\\";
            }

            return GetCloudfilesPath(dirPath);
        }


        /// <summary>
        /// Returns encoded path in UTF8
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string GetEncodedCloudfilesPath(string path)
        {
            return HttpUtility.UrlEncode(path, Encoding.UTF8);
        }


        /// <summary>
        /// Returns relative path
        /// </summary>
        /// <param name="path">Path for which relative path will be generated</param>
        /// <returns>String of relative path</returns>
        public static string GetRelativePath(string path)
        {
            var current = new Directory().GetCurrentDirectory();
            path = (path.StartsWith(current) ? path.Substring(current.Length) : path).ToLowerInvariant();
            path = path.Length > 1 && path[1] == ':' ? path.Remove(1, 1) : path;
            path = path.StartsWith("\\") ? path.Remove(0, 1) : path;
            return path;
        }


        /// <summary>
        /// Returns absolute path of path
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string GetAbsolutePath(string path)
        {
            if (path.Contains(@":\"))
            {
                return path;
            }
            if (path.StartsWith("~"))
            {
                path = path.Remove(0, 1);
            }
            return SettingsKeyProvider.WebApplicationPhysicalPath + path.Replace('/', '\\');
        }


        /// <summary>
        /// 
        /// </summary>
        public static TOut ConvertEnum<TIn, TOut>(TIn input)
            where TIn : struct
            where TOut : struct
        {
            TOut output;
            if (!Enum.TryParse<TOut>(input.ToString(), true, out output))
            {
                throw new InvalidCastException();
            }
            return output;
        }


        /// <summary>
        /// Validate path. Throws exception if there are some problems
        /// </summary>
        /// <param name="path"></param>
        public static void ValidatePath(string path)
        {
            if (path == null)
                throw new ArgumentNullException("path");
            if (String.IsNullOrWhiteSpace(path))
                throw new ArgumentException("can't be blank", "path");
        }


        /// <summary>
        /// Validation of directory exists.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="dir"></param>
        public static void ValidateDirectoryExists(string path, Directory dir)
        {
            if (!dir.Exists(path))
            {
                throw new System.IO.DirectoryNotFoundException(String.Format("{0} not found", path));
            }
        }


        /// <summary>
        /// Validation of path length
        /// </summary>
        /// <param name="path"></param>
        public static void ValidatePathLength(string path)
        {
            string cloudPath = Helpers.GetCloudfilesPath(path);

            try
            {
                Common.ValidateObjectName(cloudPath);
            }
            catch (ObjectNameException ex)
            {
                throw new System.IO.PathTooLongException(String.Format("The path: '{0}' is too long.", path), ex);
            }
        }


        /// <summary>
        /// Validate length of directory path
        /// </summary>
        /// <param name="path"></param>
        public static void ValidateDirectoryPathLength(string path)
        {
            string encodedPath = Helpers.GetCloudfilesDirectoryPath(path);

            try
            {
                Common.ValidateObjectName(encodedPath);
            }
            catch (ObjectNameException ex)
            {
                throw new System.IO.PathTooLongException(String.Format("The path: '{0}' is too long.", path), ex);
            }
        }


        /// <summary>
        /// Validation of buffer size
        /// </summary>
        /// <param name="bSize"></param>
        public static void ValidateBufferSize(int bSize)
        {
            if (bSize < 0)
            {
                throw new ArgumentOutOfRangeException("Buffer size cannot be negative");
            }
        }


        /// <summary>
        /// Validation of file mode
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="path"></param>
        /// <param name="file"></param>
        public static void ValidateFileMode(FileMode mode, string path, File file)
        {
            if ((mode == FileMode.Truncate || mode == FileMode.Open) && !file.Exists(path))
            {
                throw new System.IO.FileNotFoundException("The requested file does not exist.");
            }
            if (mode == FileMode.CreateNew && file.Exists(path))
            {
                throw new System.IO.IOException("The requested file already exists.");
            }
        }


        /// <summary>
        /// Validate whether file exists. Throws an exception if not.
        /// </summary>
        /// <param name="path">Path</param>
        /// <param name="file">Rackspace file</param>
        public static void ValidateFileExists(string path, CMS.IO.Rackspace.File file)
        {
            if (!file.Exists(path))
            {
                throw new System.IO.FileNotFoundException(String.Format("{0} not found", path));
            }
        }

        #endregion
    }
}
