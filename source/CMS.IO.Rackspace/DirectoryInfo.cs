﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;

using CMS.IO;
using CMS.IO.Rackspace.Adapter;

using Rackspace.Cloudfiles;


namespace CMS.IO.Rackspace
{

    /// <summary>
    /// Sample of DirectoryInfo class object of CMS.IO provider.
    /// </summary>
    class DirectoryInfo : CMS.IO.DirectoryInfo
    {
        #region "Variables"

        private DateTime mLastWriteTime = ZeroTimeUTC();
        private DateTime mCreationTime = ZeroTimeUTC();
        private bool mExists = false;

        private IContainerFactory mContainerFactory;
        private Container mContainer;

        #endregion


        #region "Constructors"

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="path">Path to directory</param>
        public DirectoryInfo(string path)
            : this(path, new CFContainerFactory())
        { }


        public DirectoryInfo(string path, IContainerFactory factory)
        {
            if (String.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException("path");
            }
            if (factory == null)
            {
                throw new ArgumentNullException("factory");
            }

            mContainerFactory = factory;
            mContainer = factory.GetContainer();

            string cloudPath = Helpers.GetCloudfilesDirectoryPath(path);

            StorageObject obj = mContainer.GetObject(Helpers.GetEncodedCloudfilesPath(cloudPath));

            Metadata parsed = Metadata.Parse(obj.Metadata);

            CreationTime = parsed.Creation_Time;
            LastWriteTime = parsed.Last_Write_Time;
            Exists = true;

            FullName = path;
            Name = CMS.IO.Path.GetFileName(FullName);
        }

        #endregion


        #region "Public properties"

        /// <summary>
        /// Full name of directory (whole path).
        /// </summary>
        public override string FullName
        {
            get;
            set;
        }


        /// <summary>
        /// Last write time to directory.
        /// </summary>
        public override DateTime LastWriteTime
        {
            get
            {
                return mLastWriteTime;
            }
            set
            {
                mLastWriteTime = value;
            }
        }


        /// <summary>
        /// Name of directory (without path).
        /// </summary>
        public override string Name
        {
            get;
            set;
        }


        /// <summary>
        /// Creation time.
        /// </summary>
        public override DateTime CreationTime
        {
            get
            {
                return mCreationTime;
            }
            set
            {
                mCreationTime = value;
            }
        }


        /// <summary>
        /// Whether directory exists.
        /// </summary>
        public override bool Exists
        {
            get
            {
                return mExists;
            }
            set
            {
                mExists = value;
            }
        }


        /// <summary>
        /// Parent directory.
        /// </summary>
        public override CMS.IO.DirectoryInfo Parent
        {
            get
            {
                string parentDirectoryPath = CMS.IO.Path.GetDirectoryName(FullName);
                return new DirectoryInfo(parentDirectoryPath);
            }
        }

        #endregion


        #region "Methods"

        /// <summary>
        /// Creates subdirectory.
        /// </summary>
        /// <param name="subdir">Subdirectory to create.</param>
        public override CMS.IO.DirectoryInfo CreateSubdirectory(string subdir)
        {
            return new Directory(mContainerFactory).CreateDirectory(this.FullName + "\\" + subdir);
        }


        /// <summary>
        /// Deletes directory
        /// </summary>
        public override void Delete()
        {
            new Directory(mContainerFactory).Delete(FullName, true);
        }


        /// <summary>
        /// Returns the subdirectories of the current directory.
        /// </summary>        
        public override CMS.IO.DirectoryInfo[] GetDirectories()
        {
            return GetDirectories("*");
        }


        /// <summary>
        /// Returns the subdirectories of the current directory.
        /// </summary>
        /// <param name="searchPattern">Search pattern.</param>        
        public override CMS.IO.DirectoryInfo[] GetDirectories(string searchPattern)
        {
            if (String.IsNullOrEmpty(searchPattern))
            {
                throw new ArgumentNullException("searchPattern");
            }
            if (!Exists)
            {
                throw new System.IO.DirectoryNotFoundException(String.Format("{0} doesn't exist"));
            }
            
            Directory dir = new Directory(mContainerFactory);
            return dir.GetDirectories(FullName, searchPattern)
                .Select(o => new DirectoryInfo(o, mContainerFactory))
                .ToArray();
        }


        /// <summary>
        /// Returns files of the current directory.
        /// </summary>        
        public override CMS.IO.FileInfo[] GetFiles()
        {
            return GetFiles("*");
        }


        /// <summary>
        /// Returns files of the current directory.
        /// </summary>
        /// <param name="searchPattern">Search pattern.</param>        
        public override CMS.IO.FileInfo[] GetFiles(string searchPattern)
        {
            return GetFiles(searchPattern, CMS.IO.SearchOption.TopDirectoryOnly);
        }


        /// <summary>
        /// Returns files of the current directory.
        /// </summary>
        /// <param name="searchPattern">Search pattern.</param>
        /// <param name="searchOption">Whether return files from top directory or also from any subdirectories.</param>
        public override CMS.IO.FileInfo[] GetFiles(string searchPattern, CMS.IO.SearchOption searchOption)
        {
            if (String.IsNullOrEmpty(searchPattern))
            {
                throw new ArgumentNullException("searchPattern");
            }
            if (!Exists)
            {
                throw new System.IO.DirectoryNotFoundException(String.Format("{0} doesn't exist"));
            }
            
            // Get files from current directory
            Directory dir = new Directory(mContainerFactory);
            FileInfo[] files = dir.GetFiles(FullName, searchPattern)
                .Select(o => new FileInfo(o))
                .ToArray();

            if (searchOption == CMS.IO.SearchOption.AllDirectories)
            {
                foreach (string d in dir.GetDirectories(FullName))
                {
                    DirectoryInfo info = new DirectoryInfo(d);
                    files.Concat(info.GetFiles(searchPattern, searchOption));
                }
            }

            return files;
        }

        #endregion


        #region "Private methods"

        private static DateTime ZeroTimeUTC()
        {
            return new DateTime(1601, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        }

        #endregion

    }
}
