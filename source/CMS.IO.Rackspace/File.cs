﻿using System;
using System.Text;
using System.Security.AccessControl;
using System.Configuration;
using System.Collections.Generic;

using CMS.IO;
using CMS.GlobalHelper;
using CMS.IO.Rackspace.Adapter;

using Rackspace.Cloudfiles;
using Rackspace.Cloudfiles.Constants;

using StorageHelper = CMS.IO.StorageHelper;


namespace CMS.IO.Rackspace
{
    public class File : CMS.IO.AbstractFile
    {
        #region Variables
        
        /// <summary>
        /// Container
        /// </summary>
        private readonly IContainerFactory mAdapterFactory;

        #endregion


        #region Private methods

        /// <summary>
        /// If object exists on cloud
        /// </summary>
        /// <param name="str">string path to file</param>
        /// <returns></returns>
        private bool ExistsOnCloud(string path)
        {
            try
            {
                GetStorageObject(path);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }


        /// <summary>
        /// Converts string to an array of bytes
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }


        /// <summary>
        /// Converts an array of bytes to string
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        private static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }


        /// <summary>
        /// Removes object from cache
        /// </summary>
        /// <param name="bytes">Path to object</param>
        private void RemoveFromCache(string path)
        {
            //remove the object from cache
            if (RequestStockHelper.Contains(Cache.Object + "|" + Helpers.GetCloudfilesPath(Helpers.GetEncodedCloudfilesPath(path))))
            {
                RequestStockHelper.Remove(Cache.Object + "|" + Helpers.GetCloudfilesPath(Helpers.GetEncodedCloudfilesPath(path)),false);
                CMS.IO.File.LogFileOperation(path,"Deleted from cache");
            }

            //remove Object Lists from cache cause the object can be still stored there,  proste neviem ako to z toho listu vytiahnut len ten jeden file a zmazat ho
            RequestStockHelper.Clear(Cache.ObjectList);

            //remove from FS
            string tempPath = Helpers.GetTempDirectory() + Helpers.GetRelativePath(path);       //construct route to file saved in FS(cache)
            if(System.IO.File.Exists(tempPath)){
                System.IO.File.Delete(tempPath);
                CMS.IO.File.LogFileOperation(path, "Deleted from FS");
            }

            //delete etagFile too
            if (System.IO.File.Exists(tempPath + ".etag"))
            {
                System.IO.File.Delete(tempPath + ".etag");
                CMS.IO.File.LogFileOperation(path, "Deleted from FS etag file");
            }


        }


        /// <summary>
        /// Returns storage object (Specific for RackSpace cloud files)
        /// </summary>
        /// <param name="path">Path to file (absolute path)</param>
        /// <returns>Storage object</returns>
        private StorageObject GetStorageObject(string path)
        {
            if (String.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentNullException("path");
            }

            Container container = mAdapterFactory.GetContainer();
            string cloudPath = Helpers.GetCloudfilesPath(path);

            try
            {
                return container.GetObject(
                    Helpers.GetEncodedCloudfilesPath(cloudPath));
            }
            catch (Exception e)
            {
                throw new System.IO.IOException(e.Message);
            }
        }

        #endregion


        #region Constructors
        
        /// <summary>
        /// Constructor of file without parameters
        /// </summary>
        public File()
            : this(new CFContainerFactory())
        { }


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="adapterFactory"></param>
        public File(IContainerFactory adapterFactory)
        {
            mAdapterFactory = adapterFactory;
        }

        #endregion


        #region Methods
        
        /// <summary>
        /// Determines whether the specified file exists.
        /// </summary>
        /// <param name="path">Path to file.</param>  
        public override bool Exists(string path)
        {
            return ExistsOnCloud(path);
        }


        /// <summary>
        /// Deletes the specified file. An exception is not thrown if the specified file does not exist.
        /// </summary>
        /// <param name="path">Path to file</param>
        public override void Delete(string path)
        {
            Helpers.ValidatePath(path);
            Helpers.ValidatePathLength(path);

            if (Exists(path))
            {
                try
                {
                    string cloudPath = Helpers.GetCloudfilesPath(path);
                    mAdapterFactory
                        .GetContainer()
                        .DeleteObject(Helpers.GetEncodedCloudfilesPath(cloudPath));
                }
                catch (ObjectNotFoundException)
                {
                    return;
                }
                catch (Exception e)
                {
                    throw new System.IO.IOException(e.Message);
                }

                //removes object from cache too
                RemoveFromCache(path);
            }
        }


        /// <summary>
        /// Copies an existing file to a new file. Overwriting a file of the same name is allowed.
        /// </summary>
        /// <param name="sourceFileName">Path to source file.</param>
        /// <param name="destFileName">Path to destination file.</param>
        /// <param name="overwrite">If destination file should be overwritten.</param>
        public override void Copy(string sourceFileName, string destFileName, bool overwrite)
        {
            Helpers.ValidatePath(sourceFileName);
            Helpers.ValidatePath(destFileName);

            Helpers.ValidatePathLength(sourceFileName);
            Helpers.ValidatePathLength(destFileName);

            Helpers.ValidateFileExists(sourceFileName, this);
            if (Exists(destFileName) && !overwrite)
            {
                throw new System.IO.IOException();
            }


            Container container = mAdapterFactory.GetContainer();
            try
            {
                string cloudSource = Helpers.GetCloudfilesPath(sourceFileName);
                string cloudDest = Helpers.GetCloudfilesPath(destFileName);

                //creates directory structure if doesnt exists
                Directory d = new Directory(mAdapterFactory);
                d.CreateDirectory(cloudDest.Remove(cloudDest.LastIndexOf('/')));

                //copies the objects
                container.CopyObject(
                    Helpers.GetEncodedCloudfilesPath(cloudSource),
                    Helpers.GetEncodedCloudfilesPath(cloudDest));

                //sets metadata
                StorageObject dest = GetStorageObject(destFileName);
                var metadata = Metadata.Parse(dest.Metadata);
                metadata.Last_Write_Time = DateTime.Now;
                dest.SyncMetadata(metadata.ToDictionary());
            }
            catch (Exception e)
            {
                throw new System.IO.IOException(e.Message);
            }
        }


        /// <summary>
        /// Copies an existing file to a new file. Overwriting a file of the same name is not allowed.
        /// </summary>
        /// <param name="sourceFileName">Path to source file.</param>
        /// <param name="destFileName">Path to destination file.</param>        
        public override void Copy(string sourceFileName, string destFileName)
        {
            Copy(sourceFileName, destFileName, false);
        }


        /// <summary>
        /// Moves a specified file to a new location, providing the option to specify a new file name.
        /// </summary>
        /// <param name="sourceFileName">Source file name.</param>
        /// <param name="destFileName">Destination file name.</param>
        public override void Move(string sourceFileName, string destFileName)
        {
            Copy(sourceFileName, destFileName, false);
            Delete(sourceFileName);
        }

        
        /// <summary>
        /// Opens an existing file for reading.
        /// </summary>
        /// <param name="path">Path to file.</param>
        public override CMS.IO.FileStream OpenRead(string path)
        {
            FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read);
            if (Exists(path))
            {
                return stream;
            }
            else
            {
                throw new System.IO.IOException("[File.OpenRead]: Path " + path + " does not exist.");
            }
        }

        
        /// <summary>
        /// Opens a FileStream on the specified path, with the specified mode and access.
        /// </summary>
        /// <param name="path">Path to file.</param>
        /// <param name="mode">File mode.</param>
        /// <param name="access">File access.</param>
        public override CMS.IO.FileStream Open(string path, CMS.IO.FileMode mode, CMS.IO.FileAccess access)
        {
            return new FileStream(path, mode, access);
        }

        
        /// <summary>
        /// Opens an existing UTF-8 encoded text file for reading.
        /// </summary>
        /// <param name="path">Path to file</param>
        public override CMS.IO.StreamReader OpenText(string path)
        {
            return CMS.IO.StreamReader.New(OpenRead(path).SystemStream, Encoding.UTF8);
        }

        
        /// <summary>
        /// Opens a binary file, reads the contents of the file into a byte array, and then closes the file.
        /// </summary>
        /// <param name="path">Path to file.</param>
        public override byte[] ReadAllBytes(string path)
        {
            CMS.IO.Rackspace.FileStream fs = null;
            byte[] bytes = { };

            #region ReadFromCache
            System.IO.FileStream fsReader = null;
            // First try to get data from cache
            string cachePath = Helpers.GetTempDirectory() + Helpers.GetRelativePath(path);
            string eTagFilename = cachePath + ".etag";

            if (System.IO.File.Exists(cachePath))
            {
                //if etag file does not exist, then is created
                if (!System.IO.File.Exists(eTagFilename))
                {
                    System.IO.File.Create(eTagFilename);
                }

                // Get E-tag from file
                string eTag = System.IO.File.ReadAllText(eTagFilename).Trim();

                // E-tags are the same - we can get file from cache
                string cloudEtag = GetStorageObject(path).Etag;
                if (eTag == cloudEtag)
                {
                    try
                    {
                        fsReader = new System.IO.FileStream(cachePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);

                        //returns bytes from cache
                        bytes = new byte[fsReader.Length];
                        fsReader.Read(bytes, 0, bytes.Length);

                        //log
                        CMS.IO.File.LogFileOperation(path, "Read File From Cache");

                        return bytes;
                    }
                    catch (Exception e)
                    {
                        throw new System.IO.IOException(e.ToString());
                    }
                    finally
                    {
                        //closing the stream
                        if (fs != null)
                        {
                            fsReader.Close();
                        }
                    }
                }
                else
                {
                    //puts the  etag into file
                    System.IO.File.WriteAllText(eTagFilename, cloudEtag);
                }

            }
            #endregion

            //could not acces data from cache, getting data from cloud
            if (Exists(path))
            {
                try
                {
                    fs = new CMS.IO.Rackspace.FileStream(path, FileMode.Open, FileAccess.Read);

                    bytes = new byte[fs.Length];
                    fs.Read(bytes, 0, bytes.Length);

                    //log
                    CMS.IO.File.LogFileOperation(path, "GetObjectFromCloud");
                }
                finally
                {
                    if (fs != null)
                    {
                        fs.Close();
                    }
                }
            }
            return bytes;
        }

        
        /// <summary>
        /// Opens a text file, reads all lines of the file, and then closes the file.
        /// </summary>
        /// <param name="path">Path to file.</param> 
        public override string ReadAllText(string path)
        {
            return Encoding.UTF8.GetString(ReadAllBytes(path));
        }

        
        /// <summary>
        /// Creates or overwrites a file in the specified path.
        /// </summary>
        /// <param name="path">Path to file.</param> 
        /// <returns>FileStream with readwrite permissions</returns>
        public override CMS.IO.FileStream Create(string path)
        {
            Helpers.ValidatePath(path);

            return new CMS.IO.Rackspace.FileStream(path, FileMode.Create, FileAccess.ReadWrite);
        }

        
        /// <summary>
        /// Creates or opens a file for writing UTF-8 encoded text.
        /// </summary>
        /// <param name="path">Path to file.</param>      
        public override CMS.IO.StreamWriter CreateText(string path)
        {
            Helpers.ValidatePath(path);
            
            CMS.IO.Rackspace.FileStream fs = new CMS.IO.Rackspace.FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            return CMS.IO.StreamWriter.New(fs, Encoding.UTF8);
        }

        
        /// <summary>
        /// Creates a new file, write the contents to the file, and then closes the file. If the target file already exists, it is overwritten.
        /// </summary>
        /// <param name="path">Path to file</param>
        /// <param name="contents">Content to write.</param>
        public override void WriteAllText(string path, string contents)
        {
            this.WriteAllBytes(path, GetBytes(contents));
        }


        /// <summary>
        /// Creates a new file, writes the specified byte array to the file, and then closes the file. If the target file already exists, it is overwritten.
        /// </summary>
        /// <param name="path">Path to file.</param>
        /// <param name="bytes">Bytes to write.</param>
        public override void WriteAllBytes(string path, byte[] bytes)
        {
            CMS.IO.Rackspace.FileStream fs = null;

            try
            {
                fs = new CMS.IO.Rackspace.FileStream(path, FileMode.Create, FileAccess.ReadWrite);

                fs.Write(bytes, 0, bytes.Length);
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                }
            }
        }

        
        /// <summary>
        /// Opens a file, appends the specified string to the file, and then closes the file. If the file does not exist, this method creates a file, writes the specified string to the file, then closes the file. 
        /// </summary>
        /// <param name="path">Path</param>
        /// <param name="contents">Content to write.</param>
        public override void AppendAllText(string path, string contents)
        {
            AppendAllText(path, contents, Encoding.UTF8);
        }


        /// <summary>
        /// Opens a file, appends the specified string to the file, and then closes the file. If the file does not exist, this method creates a file, writes the specified string to the file, then closes the file. 
        /// </summary>
        /// <param name="path">Path</param>
        /// <param name="contents">Content to write.</param>
        /// <param name="encoding">The character encoding to use</param>
        public override void AppendAllText(string path, string contents, Encoding encoding)
        {
            CMS.IO.Rackspace.FileStream fs = null;
            CMS.IO.StreamWriter strW = null;

            try
            {
                if (!this.Exists(path))
                {
                    strW = CreateText(path);
                }
                else
                {
                    fs = new CMS.IO.Rackspace.FileStream(path, FileMode.Append, FileAccess.ReadWrite);
                    strW = CMS.IO.StreamWriter.New(fs, encoding);
                }

                strW.Write(contents);
            }
            finally
            {
                if (strW != null)
                {
                    strW.Close();
                }
                if (fs != null)
                {
                    fs.Close();
                }
            }
        }

        
        /// <summary>
        /// Gets a FileSecurity object that encapsulates the access control list (ACL) entries for a specified directory.
        /// </summary>
        /// <param name="path">Path to file.</param>
        public override FileSecurity GetAccessControl(string path)
        {
            Helpers.ValidatePath(path);
            Helpers.ValidatePathLength(path);
            Helpers.ValidateFileExists(path, this);

            return new FileSecurity();
        }

        
        /// <summary>
        /// Returns the date and time the specified file or directory was last written to.
        /// </summary>
        /// <param name="path">Path to file.</param>
        public override DateTime GetLastWriteTime(string path)
        {
            return Metadata.Parse(GetStorageObject(path).Metadata).Last_Write_Time;
        }


        /// <summary>
        /// Sets the date and time, in coordinated universal time (UTC), that the specified file was last written to.
        /// </summary>
        /// <param name="path">Path.</param>
        /// <param name="lastWriteTimeUtc">Specified time.</param>
        public override void SetLastWriteTimeUtc(string path, DateTime lastWriteTimeUtc)
        {
            StorageObject file = GetStorageObject(path);
            var metadata = Metadata.Parse(file.Metadata);
            metadata.Last_Write_Time = lastWriteTimeUtc;
            file.SyncMetadata(metadata.ToDictionary());
        }


        /// <summary>
        /// Sets the date and time that the specified file was last written to.
        /// </summary>
        /// <param name="path">Path to file.</param>
        /// <param name="lastWriteTime">Last write time.</param>
        public override void SetLastWriteTime(string path, DateTime lastWriteTime)
        {
            StorageObject file = GetStorageObject(path);
            var metadata = Metadata.Parse(file.Metadata);
            metadata.Last_Write_Time = DateTime.Now;
            file.SyncMetadata(metadata.ToDictionary());
        }


        /// <summary>
        /// Returns actual URL of given absolute path.
        /// </summary>
        /// <param name="absolutePath">Absolute path.</param>
        [Obsolete("Use GetFileUrl(absolutePath, siteName) instead.")]
        public override string GetActualUrl(string absolutePath)
        {
            return GetStorageObject(absolutePath).StorageUrl.ToString();
        }

        
        /// <summary>
        /// Returns URL to file. If can be accessed directly then direct URL is generated else URL with GetFile page is generated.
        /// </summary>
        /// <param name="path">Virtual path starting with ~ or absolute path.</param>
        /// <param name="siteName">Site name.</param>
        public override string GetFileUrl(string path, string siteName)
        {
            string absolutePath = Helpers.GetAbsolutePath(path);
            
            //using temporary url, because file may not be public on rackspace
            string hash = CMS.GlobalHelper.ValidationHelper.GetHashString("?path=" + absolutePath);
            return URLHelper.ResolveUrl("~/CMSPages/GetRackspaceFile.aspx?path=" + absolutePath + "&hash=" + hash);
        }


        /// <summary>
        /// Sets the specified FileAttributes of the file on the specified path.
        /// </summary>
        /// <param name="path">Path to file.</param>
        /// <param name="fileAttributes">File attributes.</param>
        public override void SetAttributes(string path, CMS.IO.FileAttributes fileAttributes)
        {
            StorageObject file = GetStorageObject(path);
            var metadata = Metadata.Parse(file.Metadata);
            metadata.File_Attributes = (int)fileAttributes;
            file.SyncMetadata(metadata.ToDictionary());
        }

        
        /// <summary>
        /// Read all text.
        /// </summary>
        /// <param name="path">Path to file.</param>
        /// <param name="encoding">Encoding.</param>
        public override string ReadAllText(string path, Encoding encoding)
        {
            return encoding.GetString(ReadAllBytes(path));
        }

        
        /// <summary>
        /// Write all text.
        /// </summary>
        /// <param name="path">Path to file.</param>
        /// <param name="content">Contents.</param>
        /// <param name="encoding">Encoding</param>
        public override void WriteAllText(string path, string contents, Encoding encoding)
        {
            byte[] bytes = encoding.GetBytes(contents ?? string.Empty);
            using (FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                fs.Write(bytes, 0, bytes.Length);
            }
        }

        #endregion
    }
}
