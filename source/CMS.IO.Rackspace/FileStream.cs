﻿using System;

using CMS.IO;
using CMS.IO.Rackspace.Adapter;

using Rackspace.Cloudfiles;


namespace CMS.IO.Rackspace
{
    /// <summary>
    /// Sample of file stream of CMS.IO provider.
    /// </summary>
    public class FileStream : CMS.IO.FileStream
    {
        #region Variables

        private readonly CMS.IO.FileMode mMode;
        private readonly CMS.IO.FileAccess mAccess;
        private readonly CMS.IO.FileShare mShare;
        private readonly int mBSize;
        private readonly System.IO.FileStream mStream;
        private readonly IContainerFactory mFactory;
        private readonly string mTempPath;
        private readonly string mPath;

        #endregion 


        #region "Constructors"

        /// <summary>
        /// Initializes new instance and initializes new system file stream.
        /// </summary>
        /// <param name="path">Path to file.</param>
        /// <param name="mode">File mode.</param>
        public FileStream(string path, CMS.IO.FileMode mode)
            : this(path, mode, mode == CMS.IO.FileMode.Append ? CMS.IO.FileAccess.Write : CMS.IO.FileAccess.ReadWrite)
        {
        }


        /// <summary>
        /// Initializes new instance and initializes new system file stream.
        /// </summary>
        /// <param name="path">Path to file.</param>
        /// <param name="mode">File mode.</param>
        /// <param name="access">File access.</param>
        public FileStream(string path, CMS.IO.FileMode mode, CMS.IO.FileAccess access)
            : this(path, mode, access, CMS.IO.FileShare.Read)
        {
        }


        /// <summary>
        /// Initializes new instance and initializes new system file stream.
        /// </summary>
        /// <param name="path">Path to file.</param>
        /// <param name="mode">File mode.</param>
        /// <param name="access">File access.</param>       
        /// <param name="share">Sharing permissions.</param>
        public FileStream(string path, CMS.IO.FileMode mode, CMS.IO.FileAccess access, CMS.IO.FileShare share)
            : this(path, mode, access, share, 0x1000)
        {
        }


        /// <summary>
        /// Initializes new instance and initializes new system file stream.
        /// </summary>
        /// <param name="path">Path to file.</param>
        /// <param name="mode">File mode.</param>
        /// <param name="access">File access.</param>
        /// <param name="bSize">Buffer size.</param>
        /// <param name="share">Sharing permissions.</param>
        public FileStream(string path, CMS.IO.FileMode mode, CMS.IO.FileAccess access, CMS.IO.FileShare share, int bSize)
            : this(path, mode, access, share, bSize, new CFContainerFactory())
        {
        }


        public FileStream(string path, CMS.IO.FileMode mode, CMS.IO.FileAccess access, CMS.IO.FileShare share, int bSize, IContainerFactory factory)
            : base(path)
        {
            mMode = mode;
            mAccess = access;
            mShare = share;
            mBSize = bSize;
            mFactory = factory;

            System.IO.FileMode sMode = Helpers.ConvertEnum<FileMode, System.IO.FileMode>(mode);
            System.IO.FileAccess sAccess = Helpers.ConvertEnum<FileAccess, System.IO.FileAccess>(access);
            System.IO.FileShare sShare = Helpers.ConvertEnum<FileShare, System.IO.FileShare>(share);
            
            var file = new File(factory);

            Helpers.ValidatePath(path);
            Helpers.ValidatePathLength(path);
            Helpers.ValidateBufferSize(mBSize);
            Helpers.ValidateFileMode(mMode, path, file);

            mPath = Helpers.GetEncodedCloudfilesPath(Helpers.GetCloudfilesPath(path));
            mTempPath = (CFInfo.Instance.TempDir + GetRelativePath(path)).ToLowerInvariant().Replace("/", "\\");
          
            string directory = CMS.IO.Path.GetDirectoryName(mTempPath);

            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }

            if (file.Exists(path))
            {
                mFactory.GetContainer().GetObject(mPath).SaveToFile(mTempPath);
            }
            
            mStream = new System.IO.FileStream(mTempPath, sMode, sAccess, sShare, bSize);
        }

        #endregion


        #region "Public properties"

        /// <summary>
        /// Length of stream.
        /// </summary>
        public override long Length
        {
            get
            {
                return mStream.Length;
            }
        }


        /// <summary>
        /// Gets or sets position of current stream.
        /// </summary>
        public override long Position
        {
            get
            {
                return mStream.Position;
            }
            set
            {
                mStream.Position = value;
            }
        }

        
        /// <summary>
        /// Gets or sets system stream
        /// </summary>
        public override System.IO.Stream SystemStream
        {
            get
            {
                return mStream;
            }
        }

        #endregion


        #region "Public methods"

        /// <summary>
        /// Reads data from stream and stores them into array.
        /// </summary>
        /// <param name="array">Array where result is stored.</param>
        /// <param name="offset">Offset from begin of file.</param>
        /// <param name="count">Number of characters which are read.</param>
        public override int Read(byte[] array, int offset, int count)
        {
            return mStream.Read(array, offset, count);
        }


        /// <summary>
        /// Closes current stream.
        /// </summary>
        public override void Close()
        {
            mStream.Close();
            if ((mAccess != FileAccess.Read) && (mMode != FileMode.Open))
            {
                mFactory.GetContainer().CreateObject(mPath).WriteFromFile(mTempPath);
            }
        }


        /// <summary>
        /// Clears all buffers for this stream and causes any buffered data to be written to the underlying device.
        /// </summary>
        public override void Flush()
        {
            mStream.Flush();
        }


        /// <summary>
        /// Writes sequence of bytes to stream.
        /// </summary>
        /// <param name="buffer">Buffer.</param>
        /// <param name="offset">Offset.</param>
        /// <param name="count">Count.</param>
        public override void Write(byte[] buffer, int offset, int count)
        {
            mStream.Write(buffer, offset, count);
        }


        /// <summary>
        /// Sets the position within the current stream to the specified value.
        /// </summary>
        /// <param name="offset">Offset</param>
        /// <param name="loc">Location</param>
        public override long Seek(long offset, CMS.IO.SeekOrigin loc)
        {
            return mStream.Seek(offset, (System.IO.SeekOrigin)loc);
        }


        /// <summary>
        /// Set length to stream.
        /// </summary>
        /// <param name="value">Value to set.</param>
        public override void SetLength(long value)
        {
            mStream.SetLength(value);
        }


        /// <summary>
        /// Writes byte to the stream.
        /// </summary>
        /// <param name="value">Value to write.</param>
        public override void WriteByte(byte value)
        {
            mStream.WriteByte(value);
        }

        #endregion


        #region IDisposable Members

        /// <summary>
        /// Releases all resources.
        /// </summary>
        public override void Dispose()
        {
            Close();

            //create e-tag file
            #region create etag file

            //get etag from recently uploaded file
            string etag = mFactory.GetContainer().GetObject(Helpers.GetCloudfilesPath(mPath)).Etag;

            //write etag to file
            System.IO.File.WriteAllText(mTempPath + ".etag", etag);
            CMS.IO.File.LogFileOperation(mTempPath + ".etag", "Etag file created");
            #endregion
        }

        #endregion


        #region Private methods

        private static string GetRelativePath(string absolute)
        {
            var current = new Directory().GetCurrentDirectory();
            absolute = (absolute.StartsWith(current) ? absolute.Substring(current.Length) : absolute).ToLowerInvariant();
            absolute = absolute.Length > 1 && absolute[1] == ':' ? absolute.Remove(1, 1) : absolute;
            return absolute;
        }

        #endregion
    }
}
