﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rackspace.Cloudfiles;

namespace CMS.IO.Rackspace.Adapter
{
    /// <summary>
    /// Interface for ContainerFactory
    /// </summary>
    public interface IContainerFactory
    {
        /// <summary>
        /// Gets container, which is like provider for communication with RackSpace cloud.
        /// </summary>
        /// <returns>Returns Object container.</returns>
        Container GetContainer();
    }
}
