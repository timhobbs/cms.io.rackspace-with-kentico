﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rackspace.Cloudfiles;
using Rackspace.Cloudfiles.Constants;

namespace CMS.IO.Rackspace.Adapter
{
    /// <summary>
    /// Metadata of StorageObject
    /// </summary>
    public class Metadata
    {
        #region Properties
        
        /// <summary>
        /// Last write time of object
        /// </summary>
        public DateTime Last_Write_Time { get; set; }


        /// <summary>
        /// Creation time of object
        /// </summary>
        public DateTime Creation_Time { get; set; }


        /// <summary>
        /// Last access time of object
        /// </summary>
        public DateTime Last_Access_Time { get; set; }


        /// <summary>
        /// Indicate whether object is read only or not
        /// </summary>
        public bool Read_Only { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public int File_Attributes { get; set; }


        /// <summary>
        /// Default metadata. Returns new metadata with all date time values setted to DateTime.Now
        /// </summary>
        public static Metadata Default
        {
            get
            {
                return new Metadata()
                {
                    Creation_Time = DateTime.Now,
                    Last_Access_Time = DateTime.Now,
                    Last_Write_Time = DateTime.Now
                };
            }
        }

        #endregion


        #region Methods

        /// <summary>
        /// Puts metadata in Dictionary used in synchronization method
        /// </summary>
        /// <returns>Dictionary of Metadata</returns>
        public Dictionary<string, string> ToDictionary()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add(Headers.ObjectMetaDataPrefix + MetadataHeaders.CreationTime, Creation_Time.ToString());
            dict.Add(Headers.ObjectMetaDataPrefix + MetadataHeaders.LastAccessTime, Last_Access_Time.ToString());
            dict.Add(Headers.ObjectMetaDataPrefix + MetadataHeaders.LastWriteTime, Last_Write_Time.ToString());

            if (Read_Only)
            {
                dict.Add(Headers.ObjectMetaDataPrefix + MetadataHeaders.isReadOnly, "true");
            }
            else
            {
                dict.Add(Headers.ObjectMetaDataPrefix + MetadataHeaders.isReadOnly, "false");
            }

            dict.Add(Headers.ObjectMetaDataPrefix + MetadataHeaders.fileAtribute, File_Attributes.ToString());
            return dict;
        }

        
        /// <summary>
        /// Parse metadata from StorageObject
        /// </summary>
        /// <param name="metadata">Metadat of StorageObject</param>
        /// <returns>Metadata for CMS.IO object</returns>
        public static Metadata Parse(Dictionary<string, string> metadata)
        {
            Metadata m = new Metadata();

            if (metadata.ContainsKey(MetadataHeaders.CreationTime))
                m.Creation_Time = DateTime.Parse(metadata[MetadataHeaders.CreationTime]);

            if (metadata.ContainsKey(MetadataHeaders.LastWriteTime))
                m.Last_Write_Time = DateTime.Parse(metadata[MetadataHeaders.LastWriteTime]);

            if (metadata.ContainsKey(MetadataHeaders.LastAccessTime))
                m.Last_Access_Time = DateTime.Parse(metadata[MetadataHeaders.LastAccessTime]);

            if (metadata.ContainsKey(MetadataHeaders.isReadOnly))
            {
                if (metadata[MetadataHeaders.isReadOnly] == "true")
                {
                    m.Read_Only = true;
                }

                if (metadata[MetadataHeaders.isReadOnly] == "false")
                {
                    m.Read_Only = false;
                }

            }

            if (metadata.ContainsKey(MetadataHeaders.fileAtribute))
                m.File_Attributes = Convert.ToInt32(metadata[MetadataHeaders.fileAtribute]);

            return m;
        }

        #endregion
    }
}
