﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Rackspace.Cloudfiles;

namespace CMS.IO.Rackspace.Adapter
{
    internal class CFInfo
    {
        #region Variables

        private static CFInfo _instance;
        private readonly string _tempDir;
        private readonly string _containerName;
        public static readonly TimeSpan REFRESH_INTERVAL = TimeSpan.FromHours(23);

        #endregion


        #region Constants

        public const char DELIMITER = '/';

        #endregion


        #region Properties

        public static CFInfo Instance
        {
            get { return _instance; }
        }
       
       
        public Connection Connection
        {
            get { return _connection; }
        }

       
        public string ContainerName
        {
            get { return _containerName; }
        }
       
        
        public string TempDir
        {
            get { return _tempDir; }
        }

        #endregion


        #region Constructors

        static CFInfo()
        {
            _instance = new CFInfo();
        }

        private CFInfo()
        {
            var credentials = new UserCredentials(
                ConfigurationManager.AppSettings["CF_UserName"],
                ConfigurationManager.AppSettings["CF_ApiKey"]);
            _connection = new CF_Connection(credentials);
            _containerName = ConfigurationManager.AppSettings["CF_ContainerName"];
            _tempDir = ConfigurationManager.AppSettings["CF_TempDir"];
            System.IO.Directory.CreateDirectory(_tempDir);
        }

        #endregion

    }
}
