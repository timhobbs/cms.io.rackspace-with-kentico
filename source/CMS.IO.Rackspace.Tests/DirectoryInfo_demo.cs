﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CMS.IO.Rackspace.Adapter;
using Xunit;

namespace CMS.IO.Rackspace.Tests
{
    class DirectoryInfo_demo : TestBase
    {
        public void CreateObjectOnRackspace(string path)
        {
            IContainerFactory factory = new CFContainerFactory();
            var file = new File(factory);

            if (file.Exists(path))
            {
                file.Delete(path);
            }

            byte[] bytes = Encoding.ASCII.GetBytes("");

            CMS.IO.FileStream fileStream = file.Create(path);
            fileStream.Write(bytes, 0, bytes.Length);
            fileStream.Close();
        }
        
        public void DeleteObjectOnRackspace(string path)
        {
            IContainerFactory factory = new CFContainerFactory();
            var file = new File(factory);
            file.Delete(path);
        }

        public void TestInit()
        {
            CreateObjectOnRackspace("/Main/");
            CreateObjectOnRackspace("/Main/Sub/");
            CreateObjectOnRackspace("/Main/Sub/File.txt");
            CreateObjectOnRackspace("/Main/Sub/Fil1.txt");
            CreateObjectOnRackspace("/Main/Sub/SubKid");
            CreateObjectOnRackspace("/Main/Sub/SubKidFile.txt");
        }

        public void TestClean()
        {
            DeleteObjectOnRackspace("/Main/");
            DeleteObjectOnRackspace("/Main/Sub/");
            DeleteObjectOnRackspace("/Main/Sub/File.txt");
            DeleteObjectOnRackspace("/Main/Sub/Fil1.txt");
            DeleteObjectOnRackspace("/Main/Sub/SubKid");
            DeleteObjectOnRackspace("/Main/Sub/SubKidFile.txt");
        }
        [Fact]
        public void DirectoryInfo_Structure_Test()
        {
            Run(() =>
            {
                TestInit();
                DirectoryInfo test = new DirectoryInfo("/Main/Sub/");

                //assert
                Assert.Equal(test.Name, "Sub");
                Assert.Equal(test.FullName, "/Main/Sub/");

                Assert.Equal(test.Parent.Name, "Main");
                Assert.Equal(test.Parent.FullName, "/Main/");

                Assert.Equal(test.Parent.Parent.Name, "root");
                Assert.Equal(test.Parent.Parent.FullName, "/");

                Assert.True(test.Exists);
                Assert.True(test.Parent.Exists);
                Assert.True(test.Parent.Parent.Exists);

            });
        }
        
        [Fact]
        public void DirectoryInfo_GetDirectories_Test()
        {
            Run(() =>
            {
                TestInit();

                //arrange 
                DirectoryInfo test = new DirectoryInfo("/Main/Sub/");
                CMS.IO.DirectoryInfo[] files = test.GetDirectories();

                //assert
                Assert.Equal(files.Length, 2);
                Assert.Equal(files[0].Parent.Name, test.Name);

            });
        }

        [Fact]
        public void DirectoryInfo_GetFiles_From_CurDir_Test()
        {
            Run(() =>
            {
                TestInit();

                //arrange 
                DirectoryInfo test = new DirectoryInfo("/Main/Sub/");
                CMS.IO.FileInfo[] files = test.GetFiles();

                //assert
                Assert.Equal(files.Length, 2);
                Assert.Equal(files[0].DirectoryName, test.Name);

            });
        }

        [Fact]
        public void DirectoryInfo_GetFiles_From_SubDirs_Test()
        {
            Run(() =>
            {
                TestInit();

                //arrange 
                DirectoryInfo test = new DirectoryInfo("/Main/Sub/");
                CMS.IO.FileInfo[] files = test.GetFiles("*", CMS.IO.SearchOption.AllDirectories);

                //assert
                Assert.Equal(files.Length, 3);
                Assert.Equal(files[0].DirectoryName, test.Name);

            });
        }
    }
}
