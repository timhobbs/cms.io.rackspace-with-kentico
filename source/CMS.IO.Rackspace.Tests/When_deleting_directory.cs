﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using CMS.IO.Rackspace;
using CMS.IO.Rackspace.Adapter;

namespace CMS.IO.Rackspace.Tests
{
    public class When_deleting_directory : TestBase
    {
        [Fact]
        public void Directory_should_get_deleted()
        {
            // arrange
            string currentDir = System.IO.Directory.GetCurrentDirectory();

            IContainerFactory factory = new CFContainerFactory();
            string dirPath = currentDir + @"\directoryToDelete";
            Directory directory = new Directory(factory);

            // act
            DirectoryInfo dirInfo = directory.CreateDirectory(dirPath);
            directory.Delete(dirPath);

            // assert

            Assert.False(directory.Exists(dirPath));
        }
    }
}
