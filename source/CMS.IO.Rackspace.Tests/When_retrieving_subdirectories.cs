﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using CMS.IO.Rackspace;
using CMS.IO.Rackspace.Adapter;

namespace CMS.IO.Rackspace.Tests
{
    public class When_retrieving_subdirectories : TestBase
    {
        [Fact]
        public void Directory_should_contain_subdirectories()
        {
            // arrange
            // create directory and two subdirectories
            Directory directory = new Directory();
            string currentDir = directory.GetCurrentDirectory();

            IContainerFactory factory = new CFContainerFactory();
            string parentDirPath = currentDir + @"\parentdir";
            string subDir1Path = parentDirPath + @"\subdir1";
            string subDir2Path = parentDirPath + @"\subdir2";

            CreateDirectoryOnRackspace(parentDirPath);
            CreateDirectoryOnRackspace(subDir1Path);
            CreateDirectoryOnRackspace(subDir2Path);


            // act
            var subdirectories = directory.GetDirectories(parentDirPath);
            string[] subdirectoryNames = { subDir1Path, subDir2Path };

            // assert
            Assert.Equal<string[]>(subdirectories, subdirectoryNames);
        }

        private void CreateDirectoryOnRackspace(string path)
        {
            Directory directory = new Directory();
            if (directory.Exists(path))
            {
                directory.DeleteDirectoryStructure(path);   
            }
            DirectoryInfo dirInfo = directory.CreateDirectory(path);
        }
    }
}
