﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using CMS.SettingsProvider;
using CMS.CMSHelper;

namespace CMS.IO.Rackspace.Tests
{
    public class TestBase
    {
        static TestBase()
        {
            SettingsHelper.AppSettings["CMSExternalStorageName"] = "Rackspace";
            SettingsHelper.AppSettings["CMSStorageProviderAssembly"] = "CMS.IO.Rackspace";
            CMSContext.Init();
        }
    }
}
