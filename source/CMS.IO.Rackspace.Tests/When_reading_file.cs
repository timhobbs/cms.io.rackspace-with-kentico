﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CMS.IO.Rackspace.Adapter;
using Xunit;

namespace CMS.IO.Rackspace.Tests
{
    public class When_reading_file : TestBase
    {
        //[Fact(Skip = "Cant be tested automatically on appharbor")]
        [Fact]
        public void Content_should_be_same()
        {
            // arrange
            string currentDir = System.IO.Directory.GetCurrentDirectory();

            IContainerFactory factory = new CFContainerFactory();
            var testString = "Toto je testovaci subor 1.";
            byte[] bytes = Encoding.UTF8.GetBytes(testString);
            string path = currentDir + @"\Test1.txt";
            File file = new File(factory);
            if (file.Exists(path))
            {
                file.Delete(path);
            }
            using (var stream = file.Create(path))
            {
                stream.Write(bytes, 0, bytes.Length);
            }

            // act
            byte[] outBytes = new byte[bytes.Length];
            using (var fileStream = new FileStream(path, FileMode.Open))
            {
                fileStream.Read(outBytes, 0, bytes.Length);
            }

            // assert
            Assert.Equal(Encoding.UTF8.GetString(outBytes), testString);
        }
    }
}
