﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using CMS.IO.Rackspace.Adapter;

namespace CMS.IO.Rackspace.Tests
{
    public class When_coping_files : TestBase
    {
        [Fact]
        public void Copied_content_should_be_same()
        {
            // arrange
            string currentDir = System.IO.Directory.GetCurrentDirectory();

            IContainerFactory factory = new CFContainerFactory();
            var testString = "Toto je testovaci subor 4.";
            byte[] bytes = Encoding.UTF8.GetBytes(testString);
            string path = currentDir + @"\Test4.txt";
            string copy = currentDir + @"\Copy_Test4.txt";
            File file = new File(factory);
            if (file.Exists(path))
            {
                file.Delete(path);
            }
            if (file.Exists(copy))
            {
                file.Delete(copy);
            }
            using (var stream = file.Create(path))
            {
                stream.Write(bytes, 0, bytes.Length);
            }

            // act
            file.Copy(path, copy);

            // assert
            Assert.Equal(file.ReadAllText(copy), testString);
        }
    }
}
