﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using CMS.IO.Rackspace;
using CMS.IO.Rackspace.Adapter;
using Rackspace.Cloudfiles;

namespace CMS.IO.Rackspace.Tests
{
    public class When_creating_file : TestBase
    {
        //[Fact(Skip = "Cant be tested automatically on appharbor")]
        [Fact]
        public void File_should_get_created()
        {
            // arrange
            string currentDir = System.IO.Directory.GetCurrentDirectory();
            
            IContainerFactory factory = new CFContainerFactory();
            String testString = "Toto je testovaci subor 3.";
            byte[] bytes = Encoding.UTF8.GetBytes(testString);
            string path = currentDir + @"\Test\Test3.txt";
                
            File file = new File(factory);
            if (file.Exists(path))
            {
                file.Delete(path);
            }

            // act
            using (CMS.IO.FileStream stream = new CMS.IO.Rackspace.FileStream(path, FileMode.Create))
            {
                stream.Write(bytes, 0, bytes.Length);
            }
            
            // assert
            Assert.True(file.Exists(path));
            Assert.Equal(file.ReadAllText(path), testString);
        }
    }
}
