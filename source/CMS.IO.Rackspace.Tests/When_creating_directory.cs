﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using CMS.IO.Rackspace;
using CMS.IO.Rackspace.Adapter;

namespace CMS.IO.Rackspace.Tests
{
    public class When_creating_directory : TestBase
    {
        //[Fact(Skip = "Cant be tested automatically on appharbor")]
        [Fact]
        public void Directory_should_get_created()
        {
            // arrange
            string currentDir = System.IO.Directory.GetCurrentDirectory();

            IContainerFactory factory = new CFContainerFactory();
            string parentDirPath = currentDir + @"\directory1";
            string subDirPath = currentDir + @"\directory1\subdirectory";
            Directory directory = new Directory(factory);
            if (directory.Exists(parentDirPath))
            {
                directory.DeleteDirectoryStructure(parentDirPath);
            }

            // act
            DirectoryInfo parentDirInfo = directory.CreateDirectory(parentDirPath);
            DirectoryInfo subDirInfo = directory.CreateDirectory(subDirPath);

            // assert

            Assert.True(subDirInfo.Exists);
            Assert.Equal(true, subDirInfo.Parent.Exists);
        }
    }
}
