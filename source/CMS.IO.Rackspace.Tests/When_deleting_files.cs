﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using CMS.IO.Rackspace.Adapter;

namespace CMS.IO.Rackspace.Tests
{
    public class When_deleting_files : TestBase
    {
        [Fact]
        public void File_should_get_deleted()
        {
            // arrange
            string currentDir = new Directory().GetCurrentDirectory();
            var testString = "Toto je testovaci subor 2.";

            byte[] bytes = Encoding.UTF8.GetBytes(testString);
            string path = currentDir + @"\Test2.txt";
            File file = new File();
            file.Delete(path);

            using (var stream = file.Create(path))
            {
                stream.Write(bytes, 0, bytes.Length);
            }

            // act
            file.Delete(path);

            // assert
            Assert.Throws<System.IO.FileNotFoundException>(() =>
            {
                file.Open(path, FileMode.Open, FileAccess.Read);
            });
        }
    }
}
