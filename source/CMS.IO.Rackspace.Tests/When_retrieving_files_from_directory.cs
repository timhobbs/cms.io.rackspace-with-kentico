﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using CMS.IO.Rackspace;
using CMS.IO.Rackspace.Adapter;

namespace CMS.IO.Rackspace.Tests
{

    public class When_retrieving_files_from_directory : TestBase
    {
        //[Fact(Skip = "Cant be tested automatically on appharbor")]
        [Fact]
        public void Directory_should_contain_files()
        {
            // arrange
            // create directory and two files
            string currentDir = System.IO.Directory.GetCurrentDirectory();

            IContainerFactory factory = new CFContainerFactory();
            string dirPath = currentDir + @"\directoryWithFiles";
            Directory directory = new Directory(factory);
            if (directory.Exists(dirPath))
            {
                directory.DeleteDirectoryStructure(dirPath);
            }

            DirectoryInfo dirInfo = directory.CreateDirectory(dirPath);

            string fileName1 = dirPath + @"\file1.txt";
            string fileName2 = dirPath + @"\file2.txt";
            CreateFileOnRackspace(fileName1);
            CreateFileOnRackspace(fileName2);

            // act
            var files = directory.GetFiles(dirPath);
            string[] fileNames = { fileName1, fileName2 };

            // assert
            Assert.Equal<string[]>(fileNames, files);
        }

        public void CreateFileOnRackspace(string path)
        {
            IContainerFactory factory = new CFContainerFactory();
            String testString = "obsah";
            byte[] bytes = Encoding.UTF8.GetBytes(testString);

            File file = new File(factory);
            if (file.Exists(path))
            {
                file.Delete(path);
            }

            using (CMS.IO.FileStream stream = new CMS.IO.Rackspace.FileStream(path, FileMode.Create))
            {
                stream.Write(bytes, 0, bytes.Length);
            }
        }
    }
}
