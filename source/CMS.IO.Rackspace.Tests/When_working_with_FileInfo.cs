﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using CMS.IO.Rackspace.Adapter;
using CMS.IO.Rackspace;

namespace CMS.IO.Rackspace.Tests
{
    public class When_working_with_FileInfo : TestBase
    {
        public void CreateObjectOnRackspace(string path)
        {
            IContainerFactory factory = new CFContainerFactory();
            var file = new File(factory);
            
            if (file.Exists(path))
            {
                file.Delete(path);
            }

            byte[] bytes = Encoding.ASCII.GetBytes("Obsah");

            CMS.IO.FileStream fileStream = file.Create(path);
            fileStream.Write(bytes, 0, bytes.Length);
            fileStream.Close();
        }

        //[Fact(Skip = "Cant be tested automatically on appharbor")]
        [Fact]
        public void FileInfo_properties_should_be_same()
        {
            // arrange
            string fullName = System.IO.Directory.GetCurrentDirectory() + @"\test_properties.txt";

            CreateObjectOnRackspace(fullName);

            // act
            FileInfo file = new FileInfo(fullName);

            // assert
            Assert.Equal(5, file.Length);
            Assert.Equal(".txt", file.Extension);
            Assert.Equal("test_properties.txt", file.Name);
            Assert.True(file.Exists);
        }

        //[Fact(Skip = "Cant be tested automatically on appharbor")]
        [Fact]
        public void FileInfo_copied_object_should_be_same()
        {
            // arrange
            string currentDir = System.IO.Directory.GetCurrentDirectory();

            CreateObjectOnRackspace(currentDir + @"\copyTest.txt");
            FileInfo file = new FileInfo(currentDir + @"\copyTest.txt");

            // act
            CMS.IO.FileInfo kopia = file.CopyTo(currentDir + @"\copyTest_copy.txt", true);

            // assert
            Assert.Equal(file.Length, kopia.Length);
        }

        //[Fact(Skip = "Cant be tested automatically on appharbor")]
        [Fact]
        public void FileInfo_object_should_be_deleted()
        {
            // arrange
            string currentDir = System.IO.Directory.GetCurrentDirectory();

            CreateObjectOnRackspace(currentDir + @"\test_delete.txt");
            FileInfo file = new FileInfo(currentDir + @"\test_delete.txt");

            // act
            file.Delete();

            // assert
            Assert.Equal(false, file.Exists);
        }

        //[Fact(Skip = "Cant be tested automatically on appharbor")]
        [Fact]
        public void FileInfo_object_should_be_moved()
        {
            // arrange
            string currentDir = System.IO.Directory.GetCurrentDirectory();

            IContainerFactory factory = new CFContainerFactory();
            string originalFullname = currentDir + @"\test_moving.txt";
            string dirName = currentDir + @"\dir";
            CreateObjectOnRackspace(originalFullname);

            string destFullname = currentDir + @"\dir\test_moving.txt";
            // delete destination file from previous test...
            FileInfo oldFile = new FileInfo(destFullname);
            if (oldFile.Exists)
            {
                oldFile.Delete();
            }

            Directory dir = new Directory(factory);
            if (dir.Exists(dirName))
            {
                dir.DeleteDirectoryStructure(dirName);
            }
            DirectoryInfo dirInfo = dir.CreateDirectory(dirName);
            FileInfo file = new FileInfo(originalFullname);

            // act
            file.MoveTo(destFullname);

            // assert
            Assert.Equal(currentDir + @"\dir", file.DirectoryName);
            Assert.False(new FileInfo(originalFullname).Exists);
            Assert.True(file.Exists);
        }
    }
}

