﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------



public partial class CMSModules_Workflows_Controls_UI_WorkflowAction_Edit {
    
    /// <summary>
    /// formElem control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.FormControls.UIForm formElem;
    
    /// <summary>
    /// pnlGeneral control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.FormControls.FormCategory pnlGeneral;
    
    /// <summary>
    /// aDisplayName control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.FormControls.FormField aDisplayName;
    
    /// <summary>
    /// aName control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.FormControls.FormField aName;
    
    /// <summary>
    /// aDescription control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.FormControls.FormField aDescription;
    
    /// <summary>
    /// lblDesc control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.FormControls.FormLabel lblDesc;
    
    /// <summary>
    /// txtDesc control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSFormControls_System_LocalizableTextBox txtDesc;
    
    /// <summary>
    /// pnlImages control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.FormControls.FormCategory pnlImages;
    
    /// <summary>
    /// aIconGUID control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.FormControls.FormField aIconGUID;
    
    /// <summary>
    /// lblIcon control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.FormControls.FormLabel lblIcon;
    
    /// <summary>
    /// ucIcon control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSModules_AdminControls_Controls_MetaFiles_File ucIcon;
    
    /// <summary>
    /// aThumbnailGUID control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.FormControls.FormField aThumbnailGUID;
    
    /// <summary>
    /// lblMetaFile control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.FormControls.FormLabel lblMetaFile;
    
    /// <summary>
    /// ucFile control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSModules_AdminControls_Controls_MetaFiles_File ucFile;
    
    /// <summary>
    /// pnlAction control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.FormControls.FormCategory pnlAction;
    
    /// <summary>
    /// aAssemblyName control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.FormControls.FormField aAssemblyName;
    
    /// <summary>
    /// aClass control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.FormControls.FormField aClass;
    
    /// <summary>
    /// aAlowedObjects control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.FormControls.FormField aAlowedObjects;
    
    /// <summary>
    /// lblAllowedObjects control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.FormControls.FormLabel lblAllowedObjects;
    
    /// <summary>
    /// objectTypeSelector control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSFormControls_System_ObjectTypeSelector objectTypeSelector;
    
    /// <summary>
    /// aResourceID control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.FormControls.FormField aResourceID;
    
    /// <summary>
    /// lblResourceID control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.FormControls.FormLabel lblResourceID;
    
    /// <summary>
    /// moduleSelector control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSFormControls_SelectModule moduleSelector;
    
    /// <summary>
    /// aEnabled control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.FormControls.FormField aEnabled;
    
    /// <summary>
    /// aSubmit control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.FormControls.FormSubmit aSubmit;
}
