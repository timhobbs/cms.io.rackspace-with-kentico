﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------



public partial class CMSModules_Content_Controls_LanguageMenu {
    
    /// <summary>
    /// btnCultures control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAdminControls_UI_UniMenu_UniMenuButtons btnCultures;
    
    /// <summary>
    /// buttons control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAdminControls_UI_UniMenu_UniMenuButtons buttons;
    
    /// <summary>
    /// pnlCompare control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.PlaceHolder pnlCompare;
    
    /// <summary>
    /// splitView control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAdminControls_UI_UniMenu_UniMenuButtons splitView;
}
