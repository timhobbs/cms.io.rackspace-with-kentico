﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.5446
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------



public partial class CMSModules_ContactManagement_Controls_UI_Account_Filter {
    
    /// <summary>
    /// pnl control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Panel pnl;


    /// <summary>
    /// pnl control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.LinkButton btnReset;
    
    /// <summary>
    /// lblName control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedLabel lblName;
    
    /// <summary>
    /// fltName control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAdminControls_UI_UniGrid_Filters_TextSimpleFilter fltName;

    protected global::CMSAdminControls_UI_UniGrid_Filters_TimeSimpleFilter fltCreated;


    
    /// <summary>
    /// lblAccountStatus control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedLabel lblAccountStatus;
    
    /// <summary>
    /// fltAccountStatus control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSModules_ContactManagement_FormControls_AccountStatusSelector fltAccountStatus;
    
    /// <summary>
    /// lblEmail control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedLabel lblEmail;
    
    /// <summary>
    /// fltEmail control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAdminControls_UI_UniGrid_Filters_TextSimpleFilter fltEmail;
    
    /// <summary>
    /// lblContactName control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedLabel lblContactName;
    
    /// <summary>
    /// fltContactName control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAdminControls_UI_UniGrid_Filters_TextSimpleFilter fltContactName;
    
    /// <summary>
    /// plcSite control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.PlaceHolder plcSite;
    
    /// <summary>
    /// lblSite control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedLabel lblSite;
    
    /// <summary>
    /// siteSelector control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSFormControls_Sites_SiteSelector siteSelector;
    
    /// <summary>
    /// siteOrGlobalSelector control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSFormControls_Sites_SiteOrGlobalSelector siteOrGlobalSelector;
    
    /// <summary>
    /// plcAdvancedSearch control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.PlaceHolder plcAdvancedSearch;
    
    /// <summary>
    /// lblOwner control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedLabel lblOwner;
    
    /// <summary>
    /// fltOwner control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAdminControls_UI_UniGrid_Filters_TextSimpleFilter fltOwner;
    
    /// <summary>
    /// lblCountry control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedLabel lblCountry;
    
    /// <summary>
    /// fltCountry control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAdminControls_UI_UniGrid_Filters_TextSimpleFilter fltCountry;
    
    /// <summary>
    /// lblState control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedLabel lblState;
    
    /// <summary>
    /// fltState control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAdminControls_UI_UniGrid_Filters_TextSimpleFilter fltState;
    
    /// <summary>
    /// lblCity control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedLabel lblCity;
    
    /// <summary>
    /// fltCity control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAdminControls_UI_UniGrid_Filters_TextSimpleFilter fltCity;
    
    /// <summary>
    /// lblPhone control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedLabel lblPhone;
    
    /// <summary>
    /// fltPhone control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSAdminControls_UI_UniGrid_Filters_TextSimpleFilter fltPhone;
    
    /// <summary>
    /// plcMerged control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.PlaceHolder plcMerged;
    
    /// <summary>
    /// lblMerged control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedLabel lblMerged;
    
    /// <summary>
    /// chkMerged control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedCheckBox chkMerged;
    
    /// <summary>
    /// plcChildren control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.PlaceHolder plcChildren;
    
    /// <summary>
    /// lblChildren control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedLabel lblChildren;
    
    /// <summary>
    /// chkChildren control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedCheckBox chkChildren;
    
    /// <summary>
    /// btnSearch control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedButton btnSearch;
    
    /// <summary>
    /// pnlAdvanced control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Panel pnlAdvanced;
    
    /// <summary>
    /// imgShowSimpleFilter control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Image imgShowSimpleFilter;
    
    /// <summary>
    /// lnkShowSimpleFilter control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.LinkButton lnkShowSimpleFilter;
    
    /// <summary>
    /// pnlSimple control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Panel pnlSimple;
    
    /// <summary>
    /// imgShowAdvancedFilter control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Image imgShowAdvancedFilter;
    
    /// <summary>
    /// lnkShowAdvancedFilter control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.LinkButton lnkShowAdvancedFilter;
}
