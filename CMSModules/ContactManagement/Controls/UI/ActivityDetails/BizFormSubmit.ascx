<%@ Control Language="C#" AutoEventWireup="true" Codebehind="BizFormSubmit.ascx.cs"
    Inherits="CMSModules_ContactManagement_Controls_UI_ActivityDetails_BizFormSubmit" %>
<table>
    <tr>
        <td class="ActivityDetailsLabel">
            <cms:LocalizedLabel runat="server" ID="lblRecord" ResourceString="om.activity.record"
                EnableViewState="false" DisplayColon="true" />
        </td>
        <td>
            <cms:LocalizedLinkButton runat="server" ID="btnView" ResourceString="om.activitydetails.viewrecord" />
        </td>
    </tr>
</table>
