<%@ Control Language="C#" AutoEventWireup="true" Codebehind="SubscriptionStatusFilter.ascx.cs" Inherits="CMSModules_Newsletters_Controls_SubscriptionStatusFilter" %>

<asp:Panel runat="server" ID="pnlFilter">
    <asp:DropDownList ID="ddlStatusFilter" runat="server" CssClass="DropDownField" />    
</asp:Panel>