<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSModules_Reporting_FormControls_ReportValueSelector"
    Codebehind="~/CMSModules/Reporting/FormControls/ReportValueSelector.ascx.cs" %>
<%@ Register Src="~/CMSModules/Reporting/FormControls/ReportItemSelector.ascx" TagName="BasicReportSelector"
    TagPrefix="cms" %>
<cms:BasicReportSelector ID="brsItems" runat="server" />
