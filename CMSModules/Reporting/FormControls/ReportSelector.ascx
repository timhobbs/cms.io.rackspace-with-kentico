<%@ Control Language="C#" AutoEventWireup="true" Codebehind="~/CMSModules/Reporting/FormControls/ReportSelector.ascx.cs" Inherits="CMSModules_Reporting_FormControls_ReportSelector" %>

<%@ Register Src="~/CMSModules/Reporting/FormControls/ReportItemSelector.ascx" TagName="ItemSelector" TagPrefix="cms" %>

<cms:ItemSelector runat="server" ID="ucItemSelector" ShowItemSelector = "false"/>