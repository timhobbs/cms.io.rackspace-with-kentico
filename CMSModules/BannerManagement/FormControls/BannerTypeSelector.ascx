<%@ Control Language="C#" AutoEventWireup="true" Codebehind="BannerTypeSelector.ascx.cs" Inherits="CMSModules_BannerManagement_FormControls_BannerTypeSelector" %>

<%@ Register Src="~/CMSFormControls/Basic/DropDownListControl.ascx" TagName="DropDownListControl" TagPrefix="cms" %>

<cms:DropDownListControl runat="server" ID="drpBannerType" />