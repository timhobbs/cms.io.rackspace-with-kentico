<%@ Control Language="C#" AutoEventWireup="true" Codebehind="GenderSelector.ascx.cs"
    Inherits="CMSFormControls_Selectors_GenderSelector" %>
<asp:DropDownList ID="drpGender" runat="server" CssClass="DropDownField" />
<asp:RadioButtonList ID="radGender" runat="server" Visible="false" />
