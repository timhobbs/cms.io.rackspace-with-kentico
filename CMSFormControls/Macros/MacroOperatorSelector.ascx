<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSFormControls_Macros_MacroOperatorSelector"
    Codebehind="MacroOperatorSelector.ascx.cs" %>
<%@ Register Src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" TagName="UniSelector"
    TagPrefix="cms" %>
<asp:DropDownList runat="server" ID="drpOperator" />
