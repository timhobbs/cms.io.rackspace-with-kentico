<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSFormControls_System_SelectColor" Codebehind="SelectColor.ascx.cs" %>
<div class="ColorPickerFormControl">
    <cms:ColorPicker ID="clrPicker" runat="server" SupportFolder="~/CMSAdminControls/ColorPicker" />
</div>
