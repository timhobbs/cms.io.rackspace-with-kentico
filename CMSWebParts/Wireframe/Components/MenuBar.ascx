<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSWebParts_Wireframe_Components_MenuBar" Codebehind="~/CMSWebParts/Wireframe/Components/MenuBar.ascx.cs" %>
<div class="WireframeMenuBar">
    <cms:EditableWebPartList runat="server" id="ltlItems" RenderAsTag="ul" PropertyName="Items" Type="TextArea" />
</div>