<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSWebParts_Wireframe_Forms_DropDown" Codebehind="~/CMSWebParts/Wireframe/Forms/DropDown.ascx.cs" %>
<cms:EditableWebPartList runat="server" id="ltlText" RenderAsTag="select" CssClass="WireframeDropdown" PropertyName="Items" Type="TextArea" />
<cms:WebPartResizer runat="server" id="resElem" HorizontalOnly="true" />