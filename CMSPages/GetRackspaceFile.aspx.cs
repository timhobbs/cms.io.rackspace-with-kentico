﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.UIControls;
using CMS.IO;
using CMS.GlobalHelper;
using CMS.PortalEngine;
using CMS.IO.Rackspace.Adapter;

public partial class CMSPages_GetRackspaceFile : GetFilePage
{
    #region Properties

    /// <summary>
    /// Gets or sets whether cache is allowed. By default cache is allowed on live site.
    /// </summary>
    public override bool AllowCache
    {
        get
        {
            if (mAllowCache == null)
            {
                mAllowCache = (ViewMode == ViewModeEnum.LiveSite);
            }

            return mAllowCache.Value;
        }
        set
        {
            mAllowCache = value;
        }
    }
    
    #endregion

    #region Page Events
    protected void Page_Load(object sender, EventArgs e)
    {
        string hash = QueryHelper.GetString("hash", string.Empty);
        string path = QueryHelper.GetString("path", string.Empty);

        if (ValidationHelper.ValidateHash("?path=" + path, hash, false))
        {
            if (path.StartsWith("~"))
            {
                path = Server.MapPath(path);
            }

            CMS.IO.Rackspace.FileInfo fi = new CMS.IO.Rackspace.FileInfo(path);
            
            IContainerFactory factory = new CFContainerFactory();
            CMS.IO.Rackspace.File file = new CMS.IO.Rackspace.File(factory);

            if(file.Exists(path))
            {
                CookieHelper.ClearResponseCookies();
                Response.Clear();

                SetRevalidation();

                DateTime lastModified = file.GetLastWriteTime(path);
                string etag = "\""+ lastModified.ToString() + "\"";

                // Client caching - only on the live site
                if (AllowCache && AllowClientCache && ETagsMatch(etag, lastModified))
                {
                    // Set the file time stamps to allow client caching
                    SetTimeStamps(lastModified);

                    RespondNotModified(etag, true);
                    return;
                }

                Stream stream = CMS.IO.Rackspace.FileStream.New(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

                Response.ContentType = MimeTypeHelper.GetMimetype(Path.GetExtension(path));
                SetDisposition(Path.GetFileName(path), Path.GetExtension(path));
                
                // Setup Etag property
                ETag = etag;

                if (AllowCache)
                {
                    // Set the file time stamps to allow client caching
                    SetTimeStamps(lastModified);
                    Response.Cache.SetETag(etag);
                }
                else
                {
                    SetCacheability();
                }

                SetTimeStamps(lastModified);
                Response.Flush();

                Byte[] buffer = new Byte[DataHelper.BUFFER_SIZE];
                int bytesRead = stream.Read(buffer, 0, DataHelper.BUFFER_SIZE);

                // Copy data from blob stream to cache
                while (bytesRead > 0)
                {
                    // Write the data to the current output stream
                    Response.OutputStream.Write(buffer, 0, bytesRead);

                    // Flush the data to the output
                    Response.Flush();

                    // Read next part of data
                    bytesRead = stream.Read(buffer, 0, DataHelper.BUFFER_SIZE);
                }

                stream.Close();
                CompleteRequest();
            }
            else
            {
                NotFound();
            }
        }
        else
        {
            URLHelper.Redirect(ResolveUrl("~/CMSMessages/Error.aspx?title=" + ResHelper.GetString("general.badhashtitle") + "&text=" + ResHelper.GetString("general.badhashtext")));
        }
    }
    #endregion

    #region "Methods"

    /// <summary>
    /// Sets the last modified and expires header to the response
    /// </summary>
    /// <param name="lastModified">Last modified date time.</param>
    private void SetTimeStamps(DateTime lastModified)
    {
        DateTime expires = DateTime.Now;

        // Send last modified header to allow client caching
        Response.Cache.SetLastModified(lastModified);

        Response.Cache.SetCacheability(HttpCacheability.Public);
        if (AllowClientCache)
        {
            expires = DateTime.Now.AddMinutes(ClientCacheMinutes);
        }

        Response.Cache.SetExpires(expires);
    }

    #endregion
}