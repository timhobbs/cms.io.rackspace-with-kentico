﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------



public partial class CMSSiteManager_Sites_Site_Edit_DomainAlias_Edit {
    
    /// <summary>
    /// lblDomainName control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedLabel lblDomainName;
    
    /// <summary>
    /// txtDomainName control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.CMSTextBox txtDomainName;
    
    /// <summary>
    /// rfvDomainName control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.CMSRequiredFieldValidator rfvDomainName;
    
    /// <summary>
    /// lblVisitorCulture control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedLabel lblVisitorCulture;
    
    /// <summary>
    /// cultureSelector control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSFormControls_Cultures_SiteCultureSelector cultureSelector;
    
    /// <summary>
    /// lblDefaultAliasPath control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedLabel lblDefaultAliasPath;
    
    /// <summary>
    /// pageSelector control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMSModules_Content_FormControls_Documents_SelectSinglePath pageSelector;
    
    /// <summary>
    /// lblRedirectUrl control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.LocalizedLabel lblRedirectUrl;
    
    /// <summary>
    /// txtRedirectUrl control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.ExtendedControls.CMSTextBox txtRedirectUrl;
    
    /// <summary>
    /// btnOk control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::CMS.FormControls.FormSubmitButton btnOk;
}
